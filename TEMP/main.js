document.addEventListener("DOMContentLoaded", () => 
  {
    console.log(getComputedStyle(document.body).marginTop);
    const meta = document.createElement('meta');
    meta.setAttribute("id", "test");
    document.head.append(meta);
    const element = document.getElementById('test');
    const extractedStyles = getComputedStyle(element).fontFamily;
    document.getElementById('result').textContent = extractedStyles;
  });

//console.log(getComputedStyle(document.body).marginTop);
