# How to Contribute to this Repository

## Pre-requisites

* fork or clone this repository
* the original [Runestone source](https://github.com/RunestoneInteractive/thinkcspy) is moving from restructuredText (deprecated) to PreTeXt. As such, only make changes in the `pretext` folder. You can learn more about [PreTeXt](https://pretextbook.org/documentation.html) syntax in the [cheatsheet](https://pretextbook.org/doc/quickref/quickref-pretext.pdf); it is a markup language very similar to XHTML. There are some gotchas that I stumbled across painfully; for example, you must put list items inside a paragraph. The extension below helps!
* optional: VS Code has a PreTeXt-tools extension which is recommended

## Propose a change

* make a new branch in your repository and make and test your changes
* open a Merge Request. You can open a Draft MR to give the maintainers a heads-up that your change is underway. Make sure that the MR includes a brief description of the changes.
* make sure that you add your name as a contributor to  `\pretext\FrontBackMatter\prefaceDawson.ptx`
* push your branch, and remove the Draft when ready to merge.
* a conversation may ensue in the MR through comments.
* Once all changes are approved, they will be merged into `master`. Thanks for the contribution!


## Using your own copy

You may not want to make any changes to the source document, but make modifications to suit your style of teaching. In that case, there is no need to open an MR. Please ensure that all past contributors remain named in the Preface files to meet the licensing requirements.