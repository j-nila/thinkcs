<?xml version="1.0"?>
<section xml:id="general-intro_executing-python-in-this-book">
  <title>Executing Python in this Book</title>
  <!--video xml:id="codelensvid" youtube="LZ7H1X8ar9E" width="auto"/-->
  <p>This book provides two special ways to execute Python code.  Both techniques are designed to assist you as you
            learn the Python programming language.  They will help you increase your understanding of how Python programs work.</p>
  <p>First, you can write, modify, and execute programs using an <term>activecode</term> interpreter that allows you to execute Python code 
            in the text itself (right from the web browser).  Although this is not the way real programs are written, it provides an excellent
            environment for learning a programming language like Python since you can experiment with the language as you are reading.</p>
  <program xml:id="ch01_1" interactive="activecode" language="python" codelens="no">
    <input>
print("My first program adds two numbers, 2 and 3:")
print(2 + 3)
    </input>
  </program>
  <p>Take a look at the activecode interpreter in action. You should see a
            green <em>Save &amp; Run</em> or <em>Run</em> button.             Press the button to run the code.</p>
  <p>Now modify the activecode program shown above.
            First, modify the string in the first print statement
            by changing the word <em>adds</em> to the word <em>multiplies</em>.
            Now press <em>Run</em>. You can see that the result of the program has changed.
            However, it still prints <q>5</q> as the answer.
            Modify the second print statement by changing the addition symbol,
            the <q>+</q>, to the multiplication symbol, <q>*</q>.
            Press <em>Run</em> to see the new results.</p>
  <note>Note that you lose all modifications after you leave the web page. If you want to remember what you did in the book problems, copy and paste your answers into a separate study file.</note>
  <p>After you have modified and run your code the first time,
            the slider next to the <em>Run</em> button is usable.
            You can drag the box on the slider to switch to other versions you ran.</p>
<p>The second way to execute Python code in this book is with the assistance
              of a visualization tool created by Philip Guo.  This tool, known as <term>PythonTutor</term>,
              allows you to control the step by step execution of a program.
              It also lets you see the values of
              all variables (introduced in <xref ref="simple-python-data_variables"/>
  ) as they are created and modified.
              The following example shows PythonTutor in action on the same program as we saw above.
              Note that in activecode, the source code executes
              from beginning to end and you can see the final result.
              In PythonTutor you can see and control the step-by-step progress.
              The red arrow always points to the <alert>next</alert> line of code that is going to be executed.
              The light green arrow points to the line that was <alert>just</alert> executed.</p>
              <interactive iframe="https://j-nila.gitlab.io/pytutor/1.1first.html" width="100%"/>
   <p>
    <term>Check your understanding</term>
  </p>
  <exercise label="question1_3_1">
    <statement>
      <p>The activecode interpreter allows you to (select all that apply):</p>
    </statement>
    <choices>
      <choice>
        <statement>
          <p>save programs and reload saved programs.</p>
        </statement>
        <feedback><p>
                    You should save the contents of the activecode window in another file if you want to refer to your work later.
        </p></feedback>
      </choice>
      <choice correct="yes">
        <statement>
          <p>type in Python source code.</p>
        </statement>
        <feedback><p>
                    You are not limited to running the examples that are already there.  Try   adding to them and creating your own.
        </p></feedback>
      </choice>
      <choice correct="yes">
        <statement>
          <p>execute Python code right in the text itself within the web browser.</p>
        </statement>
        <feedback><p>
                    The activecode interpreter will allow you type Python code into the text box and then you can see it execute as the interpreter interprets and executes the source code.
        </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>receive a yes/no answer about whether your code is correct or not.</p>
        </statement>
        <feedback><p>
                    Although you can (and should) verify that your code is correct by examining its output, activecode will not directly tell you whether you have correctly implemented your program.
        </p></feedback>
      </choice>
    </choices>
  </exercise>
 
</section>
