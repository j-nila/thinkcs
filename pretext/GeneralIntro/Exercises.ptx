<?xml version="1.0"?>
<exercises xml:id="general-intro_exercises">
  <title>Exercises</title>
  <exercise>
      <statement>
        <p>
            This exercise will let you practice displaying information.  Write a program that prints following:
        </p>
        <ul>
            <li>Your name.</li>
            <li>Your favorite color.</li>
            <li>Your favorite word, in double quotes.</li>
        </ul>

        <p>Use complete sentences and blank lines for readability.  To print a blank line, use:</p>
        <program language="python">
            <input>
                print()
            </input>
        </program>
        
        <p>The parentheses are required, even though there is nothing between them. If you don't believe me, leave them out and see what the output looks like!</p>

        <p>Here is what the output of a possible solution looks like:</p>

        <console><output>
My name is Jaya Papaya.
My favorite colour is purple.
My favorite word is "giraffe".
    </output></console>
</statement>
    <program interactive="activecode" language="python" xml:id="ch01_ex1_editor" codelens="no">
      <input>

      </input>
    </program>
    
    <hint>
    <p>You can use either single quotes or double quotes to enclose a string in Python.
        That lets you do something like this:
    </p>
    <program language="python">
        <input>
            print('He said "Hello!" to me.')
        </input>
    </program>
    </hint>
</exercise>

<exercise>
  <statement>
    <p>
        A person's blood type is determined by the presence or absence of antigens on the surface of their red blood cells. An antigen is a protein that can trigger an immune response when introduced into an organism where it is foreign. Agglutinogens are antigens found on red blood cells.
    </p>
    <p> The main agglutinogens are agglutinogen A, agglutinogen B and the rhesus (Rh) agglutinogen, also known as the rhesus factor or simply Rh factor.</p> 
    <p>The presence or absence of these agglutinogens differentiates the blood types. Each type is named according to the agglutinogens present on the surface of the red blood cells.</p>
    <p>Write a program that prints all the possible blood types by going through the permutations of the 3 agglutinogens. Note that at this time, you will need to print each out yourself manually. We will revisit this exercise later, to see how we can automate and generalize this based on a list of aggutinogens.</p>
    <p>
    <credit>
      <role>Source: </role>
      <entity> <url href="https://www.alloprof.qc.ca/en/students/vl/sciences/blood-types-and-compatibility-s1272#exercice-blood-compatibility">Allo Prof</url></entity>
    </credit>
    </p>
</statement>
<program interactive="activecode" language="python" xml:id="ch01_ex2_editor" codelens="no">
  <input>

  </input>
</program>

<hint>
<p>There are 3 proteins that can each be present or absent. This means that there are <m>2^3</m> or 8 different blood types.
</p>
</hint>
</exercise>
</exercises>
