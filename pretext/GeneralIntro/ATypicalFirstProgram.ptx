<?xml version="1.0"?>
<section xml:id="general-intro_a-typical-first-program">
  <title>A Typical First Program</title>
  <p>Traditionally, the first program written in a new language is called <em>Hello,
                World!</em> because all it does is display the words, Hello, World!  In Python, the source code
            looks like this.</p>
  <program language="python">
    <input>
print("Hello, World!")
</input>
  </program>
  <p>This is an example of using the <term>print function</term>, which doesn't actually
            print anything on paper. It displays a value on the screen in the terminal, and it will be the primary mechanism that we will use to <term>output</term> results or information to the user. In this case, the result is the phrase:</p>
  <pre>Hello, World!</pre>
  <p>Here is the example in activecode.  Give it a try!</p>
  <program xml:id="ch01_2" interactive="activecode" language="python" codelens="no">
    <input>
print("Hello, World!")
        </input>
  </program>
  <p>Notice the parentheses; they show that the <code>print</code> function is being <term>invoked</term>, or in other words, you are asking the print function to do something. Also notice the quotation marks around <q>Hello, World!</q>; they mark the beginning and end of the text value.
            They don't appear in the result, but are used group all the characters of the phrase.</p>
            <p>We can use <code>""</code> double-quotes or <code>''</code> single-quotes to delimit the characters of the text, as long as we use matching pairs. This is convenient if the want to print a quotation mark or apostrophe. For example:
              </p>
              <program xml:id="ch01_2_2" interactive="activecode" language="python" codelens="no">
                <input>
            print("Hello, I'm Jaya")
            print('My name rhymes with "Papaya"')
                    </input>
              </program>   
  <p>Some people judge the quality of a programming language by the simplicity of
            the Hello, World! program. By this standard, Python does about as well as
            possible.</p>
  <p>
    <term>Check your understanding</term>
  </p>
  <exercise label="question1_11_1">
    <statement>
      <p>The print function:</p>
    </statement>
    <choices>
      <choice>
        <statement>
          <p>sends information to the printer to be printed on paper.</p>
        </statement>
        <feedback><p>
                    Within the Python programming language, the print function has nothing to do with the printer.
                </p></feedback>
      </choice>
      <choice correct="yes">
        <statement>
          <p>displays a value on the screen.</p>
        </statement>
        <feedback><p>
                    Yes, the print function is used to display the value of the thing being printed.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>tells the computer to put the information in print, rather than cursive, format.</p>
        </statement>
        <feedback><p>
                    The format of the information is called its font and has nothing to do with the print function.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>tells the computer to speak the information.</p>
        </statement>
        <feedback><p>
                    That would be a different function.
                </p></feedback>
      </choice>
    </choices>
  </exercise>
</section>
