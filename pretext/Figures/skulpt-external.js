//https://jsbin.com/fododar/edit?html,js,console
//https://github.com/skulpt/skulpt/issues/1101
console.log("INSIDE");

var externalLibs = {
  "./numpy/__init__.js": "https://cdn.jsdelivr.net/gh/ebertmi/skulpt_numpy@master/numpy/__init__.js"
};
/*
      numpy : {
        path: 'https://raw.githubusercontent.com/ebertmi/skulpt_numpy/master/numpy/__init__.js',
      },
      'numpy.random' : {
        path: 'https://raw.githubusercontent.com/ebertmi/skulpt_numpy/master/numpy/random/__init__.js',
      },
      matplotlib : {
        path: 'https://raw.githubusercontent.com/ebertmi/skulpt_matplotlib/master/matplotlib/__init__.js',
      },
      'matplotlib.pyplot' : {
        path: 'https://raw.githubusercontent.com/ebertmi/skulpt_matplotlib/master/matplotlib/pyplot/__init__.js',
        dependencies: [
          'https://raw.githubusercontent.com/ebertmi/skulpt_matplotlib/master/deps/d3.min.js',
          'https://raw.githubusercontent.com/ebertmi/skulpt_matplotlib/master/deps/jquery.js',
        ],
      },
*/
function builtinRead(file) {
  console.log("Attempting file: " + Sk.ffi.remapToJs(file));
  
  if (externalLibs[file] !== undefined) {
    return Sk.misceval.promiseToSuspension(
      fetch(externalLibs[file]).then(
        function (resp){ return resp.text(); }
      ));
  }
  
  if (Sk.builtinFiles === undefined || Sk.builtinFiles.files[file] === undefined) {
    throw "File not found: '" + file + "'";
  }
  
  return Sk.builtinFiles.files[file];
}

Sk.configure({
  read: builtinRead,
  output: console.log,
  __future__: Sk.python3,
});



var module = Sk.misceval.asyncToPromise(function() {
  return Sk.importMainWithBody("<stdin>", false, [
    "import numpy as np",
    "",
    "print(dir(np))",
    ""
  ].join("\n"), true);
}).then(function() {
  console.log("HEEEEEYAAAA success");
}, function (e) {
  console.log("NOOOOOOOOOOOOOOOOOOOOOOOOO" + e.toString());
});


