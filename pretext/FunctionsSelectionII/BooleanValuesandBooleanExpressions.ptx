<?xml version="1.0"?>
<section xml:id="selection_boolean-values-and-boolean-expressions">
  <title>Boolean Values and Boolean Expressions</title>
  <video xml:id="booleanexpressions" youtube="LD-F4RODy-I" width="auto"/>
  <p>Recall that the Python type for storing true and false values is called <c>bool</c> and there are only two <term>boolean values</term>.  They are <c>True</c> and <c>False</c>.  Capitalization
            is important, since <c>true</c> and <c>false</c> are not boolean values (remember Python is case
            sensitive).</p>
  <program xml:id="ch05_1" interactive="activecode" language="python">
    <input>
print(True)
print(type(True))
print(type(False))
        </input>
  </program>
  <note>
    <p>Boolean values are not strings!</p>
    <p>It is extremely important to realize that True and False are not strings.   They are not
                surrounded by quotes.  They are the only two values in the data type <c>bool</c>.  Take a close look at the
                types shown below.</p>
  </note>
  <program xml:id="ch05_1a" interactive="activecode" language="python">
    <input>
print(type(True))
print(type("True"))
        </input>
  </program>
  <p>A <term>boolean expression</term> is an expression that evaluates to a boolean value.
            </p>
  <p>The <c>==</c> operator is one of six common <term>comparison operators</term> that we saw in the last chapter.</p>
  <program language="python">
    <input>
x != y               # x is not equal to y
x &gt; y                # x is greater than y
x &lt; y                # x is less than y
x &gt;= y               # x is greater than or equal to y
x &lt;= y               # x is less than or equal to y
</input>
  </program>
  <p>Although these operations are probably familiar to you, the Python symbols are
            different from the mathematical symbols. A common error is to use a single
            equal sign (<c>=</c>) instead of a double equal sign (<c>==</c>). Remember that <c>=</c>
            is an assignment operator and <c>==</c> is a comparison operator. Also, there is
            no such thing as <c>=&lt;</c> or <c>=&gt;</c>.</p>
  <!--With reassignment it is especially important to distinguish between an-->
  <!--assignment statement and a boolean expression that tests for equality.-->
  <!--Because Python uses the equal token (``=``) for assignment,-->
  <!--it is tempting to interpret a statement like-->
  <!--``a = b`` as a boolean test.  Unlike mathematics, it is not!  Remember that the Python token-->
  <!--for the equality operator is ``==``.-->
  <p>Note too that an equality test is symmetric, but assignment is not. For example,
            if <c>a == 7</c> then <c>7 == a</c>. But in Python, the statement <c>a = 7</c>
            is legal and <c>7 = a</c> is not. (Can you explain why?)</p>
  <p>
    <term>Check your understanding</term>
  </p>
  <exercise label="test_question6_1_1">
    <statement>
      <p>Which of the following is a Boolean expression?  Select all that apply.</p>
    </statement>
    <choices>
      <choice correct="yes">
        <statement>
          <p>True</p>
        </statement>
        <feedback><p>
                    True and False are both Boolean literals.
                </p></feedback>
      </choice>
      <choice correct="yes">
        <statement>
          <p>3 == 4</p>
        </statement>
        <feedback><p>
                    The comparison between two numbers via == results in either True or False (in this case False),  both Boolean values.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>3 + 4</p>
        </statement>
        <feedback><p>
                    3 + 4 evaluates to 7, which is a number, not a Boolean value.
                </p></feedback>
      </choice>
      <choice correct="yes">
        <statement>
          <p>3 + 4 == 7</p>
        </statement>
        <feedback><p>
                    3 + 4 evaluates to 7.  7 == 7 then evaluates to True, which is a Boolean value.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>"False"</p>
        </statement>
        <feedback><p>
                    With the double quotes surrounding it, False is interpreted as a string, not a Boolean value.  If the quotes had not been included, False alone is in fact a Boolean value.
                </p></feedback>
      </choice>
    </choices>
  </exercise>
</section>
