<?xml version="1.0"?>
<section xml:id="functions_variables-and-parameters-are-local">
  <title>Function Variables and Parameters are Local</title>
  <p>An assignment statement within a function creates a new <term>local variable</term> for the
            variable on the left hand side of the assignment operator. It is called local because this variable only
            exists inside the function and you cannot use it outside. For example,
            consider again the <c>square</c> function:</p>
  <program xml:id="bad_local" interactive="activecode" language="python">
    <input>
          def square(x):
              y = x * x
              return y
          
          z = square(10)
          print(y)
    </input>
  </program>
  <p>You will see an error message.
            When we try to use <c>y</c> on line 6 (outside the function) Python looks for a 
            variable accessible to that line of code (called <term>in scope</term>) <c>y</c> but does not find one.  This results in the
            error: <c>Name Error: 'y' is not defined.</c>
  </p>
  <subsection xml:id="subsec-fcnII-1">
  <title>Lifetime</title>
  
  
  <p>The variable <c>y</c> only exists while the function is being executed &#x2014;
            we call this its <term>lifetime</term>.
            When the execution of the function terminates (returns),
            the local variables  are destroyed.  PythonTutor helps you  visualize this
            because the local variables disappear after the function returns.  Step through the
            statements paying particular attention to the variables that are created when the function is called.
            Note when they are subsequently destroyed as the function returns.</p>
            <interactive iframe="https://j-nila.gitlab.io/pytutor/4.1scope.html" width="100%"/>
  <p>Parameters are also local and act like local variables.
            For example, the lifetime of <c>x</c> begins when <c>square</c> is
            called,
            and its lifetime ends when the function completes its execution.</p>
  <p>So it is not possible for a function to set some local variable to a
            value, complete its execution, and then when it is called again next
            time, recover the local variable.  Each call of the function creates
            new local variables, and their lifetimes expire when the function returns
            to the caller.</p>
            </subsection>
            <subsection xml:id="subsec-fcnII-2">
            <title>Scope</title>
            
            
<p>How does Python know which variables are accessible (i.e. <term>in scope</term>) to any line of code?
            First, Python looks at the variables that are defined as local variables in
            the function.  We call this the <term>local scope</term>.  If the variable name is not
            found in the local scope, then Python looks at the global variables,
            or <term>global scope</term>, in other words variables that are not defined within a function and have already been encountered when the current line of code is running.  Note that it is <em>bad practice</em> to rely on global variables; The appropriate way to code would be to pass all required values as parameters to functions.
</p>
<p>Compare the 2 functions below. Both work, but function <c>square</c> is preferred since it is does not rely on a global variable, it is standalone, and can be used in other contexts.</p>
<program xml:id="bad_vs_good_local_global" interactive="activecode" language="python">
  <input>
        def square(x):
            y = x * x
            return y

        z = square(7)
        print(z)

        num = 7

        def badSquareOf7():
            y = num * num
            return num

        z = badSquareOf7()      
        print(z)
  </input>
</program>
<interactive iframe="https://j-nila.gitlab.io/pytutor/4.2compare.html" width="100%"/>
  <p>To cement all of these ideas even further lets look at one final example.
            Inside the <c>square</c> function we are going to make an assignment to the
            parameter <c>x</c>  There's no good reason to do this other than to emphasize
            the fact that the parameter <c>x</c> is a local variable.  If you step through
            the example in PythonTutor you will see that although <c>x</c> is 0 in the local
            variables for <c>square</c>, the <c>x</c> in the global scope remains 2.  This is confusing
            to many beginning programmers who think that an assignment to a
            formal parameter will cause a change to the value of the variable that was
            used as the actual parameter, especially when the two share the same name.
            But this example demonstrates that that is clearly not how Python operates.</p>
            <interactive iframe="https://j-nila.gitlab.io/pytutor/4.2localvsglobal.html" width="100%"/>
  </subsection>
  <p>
    <term>Check your understanding</term>
  </p>
  <exercise label="test_question5_3_1">
    <statement>
      <p>What is a variable's scope?</p>
    </statement>
    <choices>
      <choice>
        <statement>
          <p>Its value</p>
        </statement>
        <feedback><p>
                    Value is the contents of the variable.  Scope concerns where the variable is "known".
        </p></feedback>
      </choice>
      <choice correct="yes">
        <statement>
          <p>The range of statements in the code where a variable can be accessed.</p>
        </statement>
        <feedback><p>
                    The range in which a variable can be accessed is it's scope. For example if a variable is defined in a function, then the scope of the variable is that function.
        </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>Its name</p>
        </statement>
        <feedback><p>
                    The name of a variable is just an identifier or alias.  Scope concerns where the variable is "known".
        </p></feedback>
      </choice>
    </choices>
  </exercise>
  <exercise label="test_question5_3_2">
    <statement>
      <p>What is a local variable?</p>
    </statement>
    <choices>
      <choice correct="yes">
        <statement>
          <p>A temporary variable that is only used inside a function</p>
        </statement>
        <feedback><p>
                    Yes, a local variable is a temporary variable that is only known (only exists) in the function it is defined in.
        </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>The same as a parameter</p>
        </statement>
        <feedback><p>
                    While parameters may be considered local variables, functions may also define and use additional local variables.
        </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>Another name for any variable</p>
        </statement>
        <feedback><p>
                    Variables that are used outside a function are not local, but rather global variables.
        </p></feedback>
      </choice>
    </choices>
  </exercise>
  <exercise label="test_question5_3_3">
    <statement>
      <p>Can you use the same name for a local variable as a global variable?</p>
    </statement>
    <choices>
      <choice>
        <statement>
          <p>Yes, and there is no reason not to.</p>
        </statement>
        <feedback><p>
                    While there is no problem as far as Python is concerned, it is generally considered bad style because of the potential for the programmer to get confused.
        </p></feedback>
      </choice>
      <choice correct="yes">
        <statement>
          <p>Yes, but it is considered bad form.</p>
        </statement>
        <feedback><p>
                    it is generally considered bad style because of the potential for the programmer to get confused.  If you must use global variables (also generally bad form) make sure they have unique names.
        </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>No, it will cause an error.</p>
        </statement>
        <feedback><p>
                    Python manages global and local scope separately and has clear rules for how to handle variables with the same name in different scopes, so this will not cause a Python error.
        </p></feedback>
      </choice>
    </choices>
  </exercise>
</section>
