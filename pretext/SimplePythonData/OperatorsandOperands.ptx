<?xml version="1.0"?>
<section xml:id="simple-python-data_operators-and-operands">
  <title>Math Operators and Operands</title>
  <introduction>
    <p>A <term>statement</term> is an instruction that the Python interpreter can execute. We
      have seen the assignment statement, and calls to the <c>print</c> function.</p>
<p xml:id="simple-python-data_index-0">An <term>expression</term> is a combination of values, variables, operators, and calls
      to functions. Expressions need to be evaluated.  In the examples below, Python first
      <term>evaluates</term> the expressions, then and displays the result in the statement.</p>
<program xml:id="ch02_13" interactive="activecode" language="python">
<input>
print(1 + 1)
print(len("hello"))
  </input>
</program>
<p>Note that <c>len</c> is a built-in Python function that returns the number
      of characters in a string.  We've previously seen the <c>print</c> , the <c>type</c> and <c>input </c>functions, so this is another example of a function!</p>
<p>The <em>evaluation of an expression</em> produces a value, which is why expressions can
      appear on the right-hand-side of assignment statements. A value all by
      itself is a simple expression, and so is a variable.  Evaluating a variable gives the value that the variable refers to.</p>
      <p>
    <term>Operators</term> are special tokens that represent computations like addition,
            multiplication and division. The values the operator works on are called
    <term>operands</term>.
  </p>
  <p>The following are all legal Python expressions whose meaning is more or less
            as you expect:</p>
  <pre>20 + 32
hour - 1
hour * 60 + minute
minute / 60
5 ** 2
(5 + 9) * (15 - 7)</pre>
  <p>The tokens <c>+</c>,    <c>-</c>, <c>*</c>and <c>/</c>, and the use of parenthesis for grouping,
            mean in Python what they mean in mathematics. The asterisk (<c>*</c>) is the
            token for multiplication, <c>/</c> for normal division, and <c>**</c> is the token for exponentiation.
            Addition, subtraction, multiplication, and exponentiation all do what you
            expect. </p>
  <program xml:id="ch02_15" interactive="activecode" language="python">
    <input>
print(2 + 3)
print(2 - 3)
print(2 * 3)
print(2 ** 3)
print(3 ** 2)
    </input>
  </program>
  <p xml:id="simple-python-data_index-1">When a variable name appears in the place of an operand, it is replaced with
            the value that it refers to before the operation is performed.
            For example, what if we wanted to convert 645 minutes into hours.
            Division is denoted by the operator token <c>/</c> which always evaluates to a floating point
            result.</p>
  <program xml:id="ch02_16" interactive="activecode" language="python">
    <input>
minutes = 645
hours = minutes / 60
print(hours)
    </input>
  </program>
</introduction>
<subsection xml:id="subsec-int-ops">
  <title>Useful integer operations</title>
  <p>What if we had wanted to know how many <em>whole</em> hours there
            are and how many minutes remain.  To help answer this question, Python gives us a second flavor of
            the division operator.  This version, called <term>integer division</term>, uses the token
    <c>//</c>.  It always <em>truncates</em> the result down to the next smallest integer.</p>
  <program xml:id="ch02_17" interactive="activecode" language="python">
    <input>
print(7 / 4)
print(7 // 4)

minutes = 645
hours = minutes // 60
print(hours)

print(6//4)
print(-6//4)
    </input>
  </program>
  <p>Pay particular attention to the first two examples above.  Notice that the result of floating point division
            is <c>1.75</c> but the result of the integer division is simply <c>1</c>.
            Take care that you choose the correct flavor of the division operator.  If
            you're working with expressions where you need floating point values, use the
            division operator <c>/</c>.  If you want an integer result, use <c>//</c>.
  </p>
  <p xml:id="simple-python-data_index-2">The <term>modulus operator</term>, sometimes also called the <term>remainder operator</term> or <term>integer remainder operator</term> works on integers (and integer expressions) and yields
            the remainder when the first operand is divided by the second. In Python, the
            modulus operator is a percent sign (<c>%</c>). The syntax is the same as for other
            operators.</p>
  <program xml:id="ch02_18" interactive="activecode" language="python">
    <input>
quotient = 7 // 3     # This is the integer division operator
print(quotient)
remainder = 7 % 3
print(remainder)
    </input>
  </program>
  <p>In the above example, 7 divided by 3 is 2 when we use integer division and there is a remainder of 1 when we use the modulus operator.</p>
  <p>The modulus operator turns out to be surprisingly useful. For example, you can
            check whether one number is divisible by another&#x2014;if <c>x % y</c> is zero, then
    <c>x</c> is divisible by <c>y</c>.
            Also, you can extract the right-most digit or digits from a number.  For
            example, <c>x % 10</c> yields the right-most digit of <c>x</c> (in base 10).
            Similarly <c>x % 100</c> yields the last two digits. You can use modulus to convert from a 24 hour time to 12 hour time <c>time%12</c>. </p>
  <p>Finally, returning to our time example, the remainder operator is extremely useful for doing conversions, say from seconds,
            to hours, minutes and seconds.
            If we start with a number of seconds, say 7684, the following program uses integer division and remainder to convert to an easier form.  Step through it to be sure you understand how the division and remainder operators are being used to
            compute the correct values.</p>
            <interactive iframe="https://j-nila.gitlab.io/pytutor/2.3integerops.html" width="100%"/>
</subsection>
<subsection xml:id="subsec-order">
  <title>Order of Operations</title>
  <p>When more than one operator appears in an expression, the order of evaluation
    depends on the <term>rules of precedence</term>. Python follows the same precedence
    rules for its mathematical operators that mathematics does.</p>
<!--The acronym PEMDAS-->
<!--is a useful way to remember the order of operations:-->
<p>
<ol marker="1">
<li>
<p>Parentheses have the highest precedence and can be used to force an
            expression to evaluate in the order you want. Since expressions in
            parentheses are evaluated first, <c>2 * (3-1)</c> is 4, and <c>(1+1)**(5-2)</c> is
            8. You can also use parentheses to make an expression easier to read, as in
            <c>(minute * 100) / 60</c>, even though it doesn't change the result.</p>
</li>
<li>
<p>Exponentiation has the next highest precedence, so <c>2**1+1</c> is 3 and
            not 4, and <c>3*1**3</c> is 3 and not 27.  Can you explain why?</p>
</li>
<li>
<p>Multiplication and both division operators have the same
            precedence, which is higher than addition and subtraction, which
            also have the same precedence. So <c>2*3-1</c> yields 5 rather than 4, and
            <c>5-2*2</c> is 1, not 6.</p>
</li>
<li>
<p>Operators with the <em>same</em> precedence (except for <c>**</c>) are
            evaluated from left-to-right. In algebra we say they are <em>left-associative</em>.
            So in the expression <c>6-3+2</c>, the subtraction happens first, yielding 3.
            We then add 2 to get the result 5. If the operations had been evaluated from
            right to left, the result would have been <c>6-(3+2)</c>, which is 1.</p>
</li>
</ol>
</p>
<!--(The-->
<!--acronym PEDMAS could mislead you to thinking that division has higher-->
<!--precedence than multiplication, and addition is done ahead of subtraction.-->
<!--Don't be misled.  Subtraction and addition are at the same precedence, and-->
<!--the left-to-right rule applies.)-->
<note>
<p>An exception to the left-to-right
        left-associative rule is the exponentiation operator **. A useful hint
        is to always use parentheses to force exactly the order you want when
        exponentiation is involved:</p>
<program xml:id="ch02_23" interactive="activecode" language="python">
<input>
print(2 ** 3 ** 2)     # the right-most ** operator gets done first!
print((2 ** 3) ** 2)   # use parentheses to force the order you want!
</input>
</program>
</note>
<!--The immediate mode command prompt of Python is great for exploring and-->
<!--experimenting with expressions like this.-->
<p>See <xref ref="appendices_operator-precedence-table"/> for <em>all</em> the operators introduced in this book.
    You will also see many upcoming non-mathematical Python operators.</p>
</subsection>
  <p>
    <term>Check your understanding</term>
  </p>
  <exercise label="test_question2_6_1">
    <statement>
      <p>What value is printed when the following statement executes?</p>
      <program language="python">
        <input>
print(18 / 4)
        </input>
      </program>
    </statement>
    <choices>
      <choice correct="yes">
        <statement>
          <p>4.5</p>
        </statement>
        <feedback><p>
                    The / operator does exact division and returns a floating point result.
        </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>5</p>
        </statement>
        <feedback><p>
                    The / operator does exact division and returns a floating point result.
        </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>4</p>
        </statement>
        <feedback><p>
                    The / operator does exact division and returns a floating point result.
        </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>2</p>
        </statement>
        <feedback><p>
                    The / operator does exact division and returns a floating point result.
        </p></feedback>
      </choice>
    </choices>
  </exercise>
  <exercise label="test_question2_6_2">
    <statement>
      <p>What value is printed when the following statement executes?</p>
      <program language="python">
        <input>
print(18 // 4)
        </input>
      </program>
    </statement>
    <choices>
      <choice>
        <statement>
          <p>4.25</p>
        </statement>
        <feedback><p>
                    - The // operator does integer division and returns an integer result
        </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>5</p>
        </statement>
        <feedback><p>
                    - The // operator does integer division and returns an integer result, but it truncates the result of the division.  It does not round.
        </p></feedback>
      </choice>
      <choice correct="yes">
        <statement>
          <p>4</p>
        </statement>
        <feedback><p>
                    - The // operator does integer division and returns the truncated integer result.
        </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>2</p>
        </statement>
        <feedback><p>
                    - The // operator does integer division and returns the result of the division on an integer (not the remainder).
        </p></feedback>
      </choice>
    </choices>
  </exercise>
  <exercise label="test_question2_6_3">
    <statement>
      <p>What value is printed when the following statement executes?</p>
      <program language="python">
        <input>
print(18 % 4)
        </input>
      </program>
    </statement>
    <choices>
      <choice>
        <statement>
          <p>4.25</p>
        </statement>
        <feedback><p>
                    The % operator returns the remainder after division.
        </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>5</p>
        </statement>
        <feedback><p>
                    The % operator returns the remainder after division.
        </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>4</p>
        </statement>
        <feedback><p>
                    The % operator returns the remainder after division.
        </p></feedback>
      </choice>
      <choice correct="yes">
        <statement>
          <p>2</p>
        </statement>
        <feedback><p>
                    The % operator returns the remainder after division.
        </p></feedback>
      </choice>
    </choices>
  </exercise>
  <exercise label="test_question2_8_1">
    <statement>
      <p>What is the value of the following expression:</p>
      <program language="python">
        <input>
16 - 2 * 5 // 3 + 1
</input>
      </program>
    </statement>
    <choices>
      <choice correct="yes">
        <statement>
          <p>14</p>
        </statement>
        <feedback><p>
                    Using parentheses, the expression is evaluated as (2*5) first, then (10 // 3), then (16-3), and then (13+1).
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>24</p>
        </statement>
        <feedback><p>
                    Remember that * has precedence over -.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>3</p>
        </statement>
        <feedback><p>
                    Remember that // has precedence over -.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>13.667</p>
        </statement>
        <feedback><p>
                    Remember that // does integer division.
                </p></feedback>
      </choice>
    </choices>
  </exercise>
  <exercise label="test_question2_8_2">
    <statement>
      <p>What is the value of the following expression:</p>
      <program language="python">
        <input>
2 ** 2 ** 3 * 3
</input>
      </program>
    </statement>
    <choices>
      <choice correct="yes">
        <statement>
          <p>768</p>
        </statement>
        <feedback><p>
                    Exponentiation has precedence over multiplication, but its precedence goes from right to left!  So 2 ** 3 is 8, 2 ** 8 is 256 and 256 * 3 is 768.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>128</p>
        </statement>
        <feedback><p>
                    Exponentiation (**) is processed right to left, so take 2 ** 3 first.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>12</p>
        </statement>
        <feedback><p>
                    There are two exponentiations.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>256</p>
        </statement>
        <feedback><p>
                    Remember to multiply by 3.
                </p></feedback>
      </choice>
    </choices>
  </exercise>
  <p>Here are animations for the above expressions:</p>
  <exercise runestone="se_tq281"/>
  <exercise runestone="se_tq282"/>
  <exercise>
    <!--TODO add a coding exercise-->
  </exercise>
</section>
