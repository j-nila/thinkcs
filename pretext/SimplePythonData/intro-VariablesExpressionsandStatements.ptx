<?xml version="1.0"?>
<section xml:id="simple-python-data_input_and_output">
  <title>Input and Output</title>
  <introduction>
  <p>In order to get started learning any programming language there are a number of
            concepts and ideas that are necessary.
            The goal of this chapter is to introduce you to the basic vocabulary of programming and some of the fundamental
            building blocks of Python.</p>
  <p>You can think of any computing device as a machine that executes instructions. We provide the instructions in the form of code. The machine takes <term>input</term> from the user or elsewhere (think of data), runs through the instructions, and produces <term>output</term> for the user to see or use.
    </p>
    <image source="SimplePythonData/Figures/input-output.png" width="50%" alt="input-output"/>
    </introduction>
<subsection xml:id="subsec-print">
  <title>The <c>print</c> function</title>
  <p>We have seen the <c>print</c> function already. The term <term>function</term> indicates a process which can be <term>passed</term> zero or more <term>arguments</term>, which does something, and which can <term>return</term> some value in its place. The <c>print</c> function specifically takes 0 or more arguments.
    </p>
    <program xml:id="ch02_intro-1" interactive="activecode" language="python">
      <input>
  print(12)
  print('hello')
  print(7 + 5)
  print() #notice the blank line in the display
  print('more', 'than', 1, 'value')
          </input>
    </program>
    <p>Note the <term>syntax</term>, or rules about how to write Python code, that you must follow:
      <ul>
        <li>
          <p>
            <c>print</c> is never capitalized. Python is case-sensitive, so <c>print</c> is not the same thing as <c>Print</c>.
          </p>
        </li>
        <li>
          <p>
            there are parentheses after <c>print</c>. In fact, whenever you want to <term>invoke</term>, or call, any function, you must use parentheses, even if you are not passing any arguments. 
          </p>
        </li>
        <li>
          <p>
            if you invoke <c>print</c> with no arguments, Python will move to the next line for the next print statement.
          </p>
          </li>
          <li>
            <p>
              if you are passing multiple arguments to any function, they must be separated by a comma <c>,</c>. 
            </p>
          </li>
          <li>
            <p>
              if you invoke <c>print</c> with multiple arguments, Python will displays the arguments in the terminal, separated by a space.
            </p>
          </li>
      </ul>
    </p>
    <p>Let's try some incorrect syntax to learn to read the error message, and fix the code! Python will stop at the first error it finds, so fix it then run your code again so that you can read the different error messages. </p>
    <program xml:id="ch02_intro-2" interactive="activecode" language="python">
      <input>
  print 12
  Print('hello')
  print
  print('more' 'than' 1 'value')
          </input>
    </program>  
    <p>Notice that line 3 didn't cause an error, but didn't print an empty line either. This is because <c>print</c> is something that Python knows about, you just aren't invoking it. Try changing line 3 to <c>Print</c> and Python will complain that it is not defined.</p>
</subsection>
<subsection xml:id="subsec-input">
  <title>The <c>input</c> function</title>
  <p>The <c>print</c> function does not return anything useful. So how do we get data from the user? We ask them with a prompt, and retrieve what they type up to the <c>enter</c> key using the <c>input</c> function.</p>
  <p>
    <c>input</c> is another example of a built-in function that we can use, so it follows some of the same rules:
    <ul>
      <li>
        <p>
          you type it as exactly <c>input</c> since Python is case-sensitive
        </p>
      </li>
      <li>
        <p>
          you follow <c>input</c> with parenthesis to invoke the function
        </p>
      </li>
      <li>
        <p>
          <c>input</c> can take 0 or 1 arguments. If there is an argument, it is the prompt that is shown on screen. (note: this book acts differently than if you run Python directly on your system or in GitHub CodeSpaces; with the book you get a pop-up window since there is no terminal).
        </p>
      </li>
      <li>
        <p>
          unlike <c>print</c>, the <c>input</c> function <term>returns</term> something to us that we probably want to use in our program. Why else would we ask the user for that data? Since we want to use it, and we don't know ahead of time what the user will give us, we need to store it in a <term>variable</term>.
        </p>
      </li>
      </ul>
  </p>
  <program xml:id="ch02_intro-3" interactive="activecode" language="python">
    <input>
user_name = input('enter your name: ')
print('hello ', user_name)
pet = input("Enter your pet's name")
print(user_name, 'has a pet named ', pet)
        </input>
  </program>
  <p>It is important to note that <c>input</c> <em>always</em> returns a string (in other words, text). We will look at converting the string to numbers in another section.</p>
</subsection>
<subsection xml:id="subsec-intro-variable">
  <title>Introduction to variables</title>
  <p>You would have noticed that we used two <term>variables</term> in the code example above. <c>user_name</c> was used to hold the value of the name input by the user, while <c>pet</c> was used for their pet's name. Variables in programming are not the same as variables that you have seen in algebra, like <code>x</code> where we try to solve for their value. In programming, a variable refers to a stored value that can change over time (hence variable), that we name so that we can refer to it elsewhere in the program. More on variables after we talk about types of values in the next section! </p>
</subsection>
<p>
  <term>Check your understanding</term>
</p>
<exercise label="var-input-pp-prompt" numbered="yes" adaptive="yes" indentation="hide" language="python">
  <statement>
    <p>Construct a block of code that asks the user for a number and prints that number.
          There is extra code to watch out for.</p>
  </statement>
  <blocks>
    <block order="5">
      <cline>prompt = 'Please enter a number: '</cline>
    </block>
    <block order="1">
      <choice correct="yes">
        <cline>user_number = input(prompt)</cline>
      </choice>
      <choice>
        <cline>user number = input(prompt) </cline>
      </choice>
    </block>
    <block order="3">
      <choice correct="yes">
        <cline>print(user_number)</cline>
      </choice>
      <choice>
        <cline>print(prompt) </cline>
      </choice>
      <choice>
      <cline>print(user) </cline>
      </choice>
    </block>
  </blocks>
</exercise>
</section>
