<?xml version="1.0"?>
<section xml:id="python-whitespace-1">
  <title>Whitespace and Readability</title>
  <p>It is good to add whitrespace (e.g. blank lines or spaces) in your code to make things more readable. There are several whitespace patterns that are used in programming, especially
    in Python. In general, follow the following rules.
    <ul>
      <li>
        <p>
          The assignment operator <c>=</c> should always be surrounded by one space on
    either side. For example, use <c>cats = 10</c> instead of <c>cats=10</c>.
        </p>
      </li>
      <li>
        <p>
The comparison operators (coming up in the next chapter) (<c>==, !=, &lt;, &gt;, &lt;=, and &gt;=</c>) should always be
    surrounded by one space on either side. For example, use <c>cats &lt;= 10</c> instead of <c>cats&lt;=10</c>.</p>
</li>
<li>
  <p>
    If arithmetic operators with different priorities are used, consider adding whitespace around the operators with the <alert>lowest priority(ies)</alert>. Use your own judgment; however, never use more than one space, and always have the same amount of whitespace on both sides of a binary operator:
    <program xml:id="whitespace_ex_1" language="python">
      <input>
# Correct:
i = i + 1
x = x*2 - 1
hypot2 = x*x + y*y
c = (a+b) * (a-b)
</input>
      </program>
  </p>
</li>
<li>
  <p>
    When you have a comma-delimited list (for example, you are invoking a function with multiple arguments), leave a space after each comma. For example use <c>print('My name is', first_name)</c> instead of <c>print('My name is',first_name)</c>
  </p>
</li>
<li>
  <p>
    Add blank lines at logical "breaks" in the code. We will see much more of this as we code more complex programs.
  </p>
</li>
</ul>
</p>
<p>
  We will see in the next chapter how indentation is really important with certain coding constructs that need to treat lines of code as a "block". For now, don't add any blank spaces at the start of a statement.
</p>

</section>
