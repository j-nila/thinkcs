<?xml version="1.0"?>
<section xml:id="python-round-and-f-strings">
  <title>Rounding and formatting numbers</title>
  <p>
    There are numerous reasons why a programmer, and a scientist in particular, may have to round computed results. Here are some of them.
    <ul>
      <li>
        <p>
          Fractions don't make sense in the context: consider fractional cents in a financial calculation.
        </p>
      </li>
      <li>
        <p>
          Scientific calculations based on measurements have other sources of error (think about the equipment that you use in your science labs), and are only valid up to a certain number of significant digits. In this case, you want to display only the significant digits (but perform calculations with all).
        </p>
      </li>
      <li>
        <p>
          As we saw, not all floating point numbers can be represented exactly (either due to issues of scale and/or internal binary representation). We call this <em>round-off error</em>: it is due to the inexact representation of real numbers. We will look at using <term>tolerances</term> in a future chapter.
        </p>
      </li>
    </ul>
  </p>
  <p>The <c>round</c> function returns a number rounded to the given digits of precision after the decimal point. If you don't indicate the number of digits, it return the nearest integer. Refer to the <url href="https://docs.python.org/3/library/functions.html#round">official documentation</url> for more details on its use.</p>  
  <program xml:id="round" interactive="activecode" language="python">
    <input>
print(round(1.278, 2)) # 2 digits after decimal
print(round(875.631, 2)) # 2 digits after decimal
print(round(1.2)) # no precision given, rounds to nearest even
print(round(1.2, -5)) # invalid precision

#rounds to nearest even number
print(round(1.5)) 
print(round(2.5))
        </input>
  </program>
<p>
  Formatted-strings, or <term>f-strings</term> are strings with an <c>f</c> directly in from of the opening quotation marks (e.g., <c>f'...'</c> or <c>f"..."</c>). Inside the f-string, you put variables inside <c>{..}</c>, and their values replace the curly brackets. More interestingly, you can format numbers to use scientific notation automatically if they are large, with a required width (e.g. the number of significant digits required). Here is a simplified <url href="https://cheatography.com/brianallan/cheat-sheets/python-f-strings-number-formatting/">cheat sheet</url> to see some of the options available. In this course, we will use the general number formatting with a given number of significant digits.
</p>
<program xml:id="f-str" interactive="activecode" language="python">
  <input>
# This program calculates the area of a circle

import math

# as measured
radius = 2.25
area = math.pi*(radius**2)

# no formatting
print('The area is', area)

# no formatting but using an f-string
print(f'The area is {area}')

# formatted to 3 digits in total
print(f'The area is {area:.3g}')
      </input>
</program>
<p>You read the f-string as:
  <ul>
    <li>
      <p>
        anything not within the curly brackets is a regular string
      </p>
    </li>
    <li>
      <p>
        Within the <c>{}</c>, the variable (or expression) to be replaced is before the <c>:</c>
      </p>
    </li>
    <li>
      <p>
        Within the <c>{}</c>, the format specifier for the variable (or expression) is after the <c>:</c>. The format specifier used here:
        <ul>
          <li>
            <p>
              the <c>.</c> is followed by the required precision, in this case 3 digits.
            </p>
          </li>
          <li>
            <p>
              the format type is <c>g</c>, which is a general float type. This means that if the number is greater than or equal to |10000|, it is shown in scientific notation, else as a decimal number. The precision indicates the number of significant digits (not necessarily digits after the decimal point)
            </p>
          </li>
        </ul>
      </p>
    </li>
  </ul>
  </p>
  <p>
    Here are two more f-string examples, one showing a fixed decimal rounding (similar to what round does), and one showing percentages.
    <program xml:id="f-str-2" interactive="activecode" language="python">
      <input>
# This program calculates the perimeter of a rectangle

# as measured
width = 60.5
height = 50.3

perimeter = 2 * (width+height)

# formatted to 1 digit after the decimal
# this is the correct significant digits, based on 
# uncertainty in measurement
print(f'The perimeter is {perimeter:.1f}')

ratio = height/width

# formatted to 1 digit after the decimal
print(f'The ratio of height to width is {ratio:.1%}')
         </input>
</program>
  </p>
  <term>f-string review</term>
  <p>
    Basic syntax: <em>f'any text { expression : width.precision format-specifier} text'</em>
    <ul>
      <li>
        <p>
          there is no space between the <c>f</c> and the quotation mark that starts the string. You must end the string with a matching quotation mark.
        </p>
      </li>
      <li>
        <p>
          the expression is within <c>{}</c> brace, or curly, brackets. It can be any defined variable, a function call, or any expression that can be evaluated. The value will be converted to a string automagically, so you don't use the <c>str()</c> function.
        </p>
      </li>
      <li>
        <p>
          You can optionally format the value if it is an int, float, or string. After the expression, within the brace brackets, the <c>:</c> colon indicates that the formatting specifier follows. The formatting specifier also has a basic syntax:
          <ul>
            <li>
              <p>
                the width is optional, it indicates the minimum number of digits. This means that any value which is smaller will be padded with spaces. Integers and floats are padded on the left, so the number is right-aligned, while strings are padded on the right so the text is left-aligned.
              </p>
            </li>
            <li>
              <p>
                the precision is optional, after a <c>.</c>. The meaning of precision varies, depending on the format specifier.
                <ul>
                  <li>
                    <p>
                      with <c>f</c> format, the precision signifies the fixed number of digits after the decimal. So <c>f'x is {x: 10.2 f}'</c> will have x with a total width of 10 spaces (including the decimal point), with 2 digits after the decimal point.
                    </p>
                  </li>
                  <li>
                    <p>
                      with the <c>g</c> format, the precision signifies the significant digits. So <c>f'x is {x: 10.2 g}'</c> will have x with a total width of 10 spaces (including the decimal point), but really only 2 digits are significant. The generic style will show either the digits, or will use scientific notation if needed. So the rest of the 10 spaces in width might be used by the scientific notation characters (e.g., 'e+04', decimal point, or leading blanks.)
                    </p>
                  </li>
                </ul>
              </p>
            </li>
          </ul>
        </p>
      </li>
    </ul>
  </p>
  <example>
    <program xml:id="f-str-3" interactive="activecode" language="python">
      <input>
# Examples only using width. Notice the numbers are not rounded.
# Also notice there are leading blanks, so all numbers end at the same place
num1 = 1234.56
num2 = 123.45
text = 'Hello'
# All have a width of 8 spaces, notice the numbers are aligned to the right
print('Examples with width of 8')
print(f'{num1:8}')
print(f'{num2:8}')
print(f'{text:8}')
print()

# These examples have width and fixed spaces after decimal
import math
print('Examples with fixed after decimal')
print(f'{math.pi:8.6f}')
print(f'{math.pi:8.3f}')
# next has no width specified, so no leading spaces
print(f'{math.pi:.3f}')
print()

# These examples use significant digits, not fixed decimal
print('Examples with generic formatting for sigfigs')
print(f'{math.pi:8.6g}')
print(f'{math.pi:8.3g}')
# next has no width specified, so no leading spaces
print(f'{math.pi:.3g}')

         </input>
</program> 
  </example>
  
  
<p>
    <term>Check your understanding</term>
  </p>
  <exercise>
    <statement>
      <p>
        Let’s say that apples are $0.40 apiece, and pears are $0.65 apiece. Write a program that asks a user how many apples and pears they want, then calculate and display the total cost. Use an f string!
      </p>
</statement>
  <program interactive="activecode" language="python" xml:id="ch02_exround_editor" codelens="no">
    <input>

    </input>
  </program>
  </exercise>
</section>
