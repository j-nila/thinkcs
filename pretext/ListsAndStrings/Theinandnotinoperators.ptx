<?xml version="1.0"?>
<section xml:id="liststrings_the-in-and-not-in-operators">
  <title>The <c>in</c> and <c>not in</c> operators</title>
  <p>The <c>in</c> operator tests if one string is a substring of another, or if an element is a member of a list.</p>
  <program xml:id="chp8_in1" interactive="activecode" language="python">
    <input>
print('p' in 'apple')
print('i' in 'apple')
print('ap' in 'apple')
print('pa' in 'apple')

fruit = ["apple", "orange", "banana", "cherry"]

print("apple" in fruit)
print("pear" in fruit)
        </input>
  </program>
  <p>Note that a string is a substring of itself, and the empty string is a
            substring of any other string. (Also note that computer scientists
            like to think about these edge cases quite carefully!).</p>
  <program xml:id="chp8_in2" interactive="activecode" language="python">
    <input>
print('a' in 'a')
print('apple' in 'apple')
print('' in 'a')
print('' in 'apple')
        </input>
  </program>
  <p>The <c>not in</c> operator returns the logical opposite result of <c>in</c>.</p>
  <program xml:id="chp8_in3" interactive="activecode" language="python">
    <input>
print('x' not in 'apple')
        </input>
  </program>
  <p>
    <term>Check your understanding</term>
  </p>
  <exercise label="test_question9_4_1">
    <statement>
      <p>What is printed by the following statements?</p>
      <program language="python">
        <input>
alist = [3, 67, "cat", [56, 57, "dog"], [ ], 3.14, False]
print(3.14 in alist)
</input>
      </program>
    </statement>
    <choices>
      <choice correct="yes">
        <statement>
          <p>True</p>
        </statement>
        <feedback><p>
                    Yes, 3.14 is an item in the list alist.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>False</p>
        </statement>
        <feedback><p>
                    There are 7 items in the list, 3.14 is one of them.
                </p></feedback>
      </choice>
    </choices>
  </exercise>
  <exercise label="test_question9_4_2">
    <statement>
      <p>What is printed by the following statements?</p>
      <program language="python">
        <input>
alist = [3, 67, "cat", [56, 57, "dog"], [ ], 3.14, False]
print(57 in alist)
</input>
      </program>
    </statement>
    <choices>
      <choice>
        <statement>
          <p>True</p>
        </statement>
        <feedback><p>
                    in returns True for top level items only.  57 is in a sublist.
                </p></feedback>
      </choice>
      <choice correct="yes">
        <statement>
          <p>False</p>
        </statement>
        <feedback><p>
                    Yes, 57 is not a top level item in alist.  It is in a sublist.
                </p></feedback>
      </choice>
    </choices>
  </exercise>
</section>
