<?xml version="1.0"?>
<section xml:id="strings_glossary">
  <title>Glossary</title>
  <glossary sorted="False">
    <gi>
      <title>aliases</title>
      <p>Multiple variables that contain references to the same object.</p>
    </gi>
    <gi>
      <title>collection data type</title>
      <p>A data type in which the values are made up of components, or elements,
                            that are themselves values.</p>
    </gi>
    <gi>
      <title>default value</title>
      <p>The value given to an optional parameter if no argument for it is
                            provided in the function call.</p>
    </gi>
    <gi>
      <title>delimiter</title>
      <p>A character or string used to indicate where a string should be split.</p>
    </gi>
    <gi>
      <title>dot notation</title>
      <p>Use of the <term>dot operator</term>, <c>.</c>, to access functions inside a
                            module, or to access methods and attributes of an object.</p>
    </gi>
    <gi>
      <title>element</title>
      <p>One of the values in a list (or other sequence). The bracket operator
                            selects elements of a list.</p>
    </gi>
    <gi>
      <title>immutable data type</title>
      <p>A data type whose values cannot be changed. Modifying functions create
                            a totally new object that does not change the original one.</p>
    </gi>
    <gi>
      <title>index</title>
      <p>A variable or value used to select a member of an ordered collection, such as
                            a character from a string, or an element from a list.</p>
    </gi>
    <gi>
    <title>list</title>
    <p>A collection of objects, where each object is identified by an index.
                          Like other types <c>str</c>, <c>int</c>, <c>float</c>, etc. there is also a
                          <c>list</c> type-converter function that tries to turn its argument into a
                          list.</p>
  </gi>
  <gi>
    <title>list traversal</title>
    <p>The sequential accessing of each element in a list.</p>
  </gi>
  <gi>
    <title>mutable data type</title>
    <p>A data type in which the elements can be modified. All mutable types
                          are compound types. Lists are mutable data types; strings are not.</p>
  </gi>
  <gi>
    <title>nested list</title>
    <p>A list that is an element of another list.</p>
  </gi>
  <gi>
    <title>object</title>
    <p>A thing to which a variable can refer.</p>
  </gi>
    <gi>
      <title>optional parameter</title>
      <p>A parameter written in a function header with an assignment to a
                            default value which it will receive if no corresponding argument is
                            given for it in the function call.</p>
    </gi>
    <gi>
      <title>pure function</title>
      <p>A function which has no side effects. Pure functions only make changes
                            to the calling program through their return values.</p>
    </gi>
    <gi>
      <title>sequence</title>
      <p>Any of the data types that consist of an ordered collection of elements, with
                            each element identified by an index.</p>
    </gi>
    <gi>
      <title>side effect</title>
      <p>A change in the state of a program made by calling a function that is
                            not a result of reading the return value from the function. Side
                            effects can only be produced by modifiers.</p>
    </gi>
    <gi>
      <title>slice</title>
      <p>A part of a string (substring) specified by a range of indices. More
                            generally, a subsequence of any sequence type in Python can be created
                            using the slice operator (<c>sequence[start:stop]</c>).</p>
    </gi>
    <gi>
      <title>string comparison (<c>&gt;, &lt;, &gt;=, &lt;=, ==, !=</c>)</title>
      <p>The six common comparision operators work with strings, evaluating according to
                            <url href="http://en.wikipedia.org/wiki/Lexicographic_order" visual="http://en.wikipedia.org/wiki/Lexicographic_order">lexigraphical order</url>.  Examples:
                            <c>'apple' &lt; 'banana'</c> evaluates to <c>True</c>.  <c>'Zeta' &lt; 'Appricot'</c>
                            evaluates to <c>False</c>.  <c>'Zebra' &lt;= 'aardvark'</c> evaluates to
                            <c>True</c> because all upper case letters precede lower case letters.</p>
    </gi>
    <gi>
      <title>in and not in operator (<c>in</c>, <c>not in</c>)</title>
      <p>The <c>in</c> operator tests whether one string is contained
                            inside another string.  Examples: <c>'heck' in "I'll be checking for
you."</c> evaluates to <c>True</c>.  <c>'cheese' in "I'll be checking for
you."</c> evaluates to <c>False</c>.</p>
    </gi>
    <gi>
      <title>traverse</title>
      <p>To iterate through the elements of a collection, performing a similar
                            operation on each.</p>
    </gi>
    <gi>
      <title>whitespace</title>
      <p>Any of the characters that move the cursor without printing visible
                            characters. The constant <c>string.whitespace</c> contains all the
                            white-space characters.</p>
    </gi>
  </glossary>
</section>
