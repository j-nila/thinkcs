<?xml version="1.0"?>
<section xml:id="strings_a-collection-data-type">
  <title>A Collection Data Type</title>
  <p>Strings and lists are different from integers, floats and boolean values because they
            are made up of smaller pieces.  In the case of strings, they are made up of smaller
            strings each containing one <term>character</term>. Lists are
            similar to strings, except that the
            elements of a list can have any type and for any one list, the items can be of different types.</p>
  <p>Types that are comprised of smaller pieces are called <term>collection data types</term>.
            Depending on what we are doing, we may want to treat a collection data type as a
            single entity (the whole), or we may want to access its parts. This ambiguity is useful.</p>
  <p>Strings can be defined as sequential collections of characters.  This means that the individual characters
            that make up the string are assumed to be in a particular order from left to right.</p>
  <p>A string that contains no characters, often referred to as the <term>empty string</term>, is still considered to be a string.  It is simply a sequence of zero characters and is represented by <c>''</c> or <c>""</c> (two single or two double quotes with nothing in between). Likewise, there is a special list that contains no elements. It is called the empty list and is denoted by <c>[]</c>. </p>
</section>
