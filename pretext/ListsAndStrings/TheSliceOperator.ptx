<?xml version="1.0"?>
<section xml:id="stringslists_the-slice-operator">
  <title>The Slice Operator</title>
  <subsection xml:id="subsec-str-slice">
  <title>The Slice Operator with strings</title>
  <p>A substring of a string is called a <term>slice</term>. Selecting a slice is similar to
            selecting a character:</p>
  <program xml:id="chp08_slice1" interactive="activecode" language="python">
    <input>
singers = "Peter, Paul, and Mary"
print(singers[0:5])
print(singers[7:11])
print(singers[17:21])
        </input>
  </program>
  <p>The slice operator <c>[n:m]</c> returns the part of the string from the n'th character
            to the m'th character, including the first but <alert>excluding</alert> the last. In other words,  start with the character at index n and
            go up to but do not include the character at index m.
            This
            behavior may seem counter-intuitive but if you recall the <c>range</c> function, it did not include its end
            point either.</p>
  <p>If you omit the first index (before the colon), the slice starts at the
            beginning of the string. If you omit the second index, the slice goes to the
            end of the string.</p>
  <p>There is no Index Out Of Range exception for a slice.
            A slice is forgiving and shifts any offending index to something legal.</p>
  <program xml:id="chp08_slice2" interactive="activecode" language="python">
    <input>
fruit = "banana"
print(fruit[:3])
print(fruit[3:])
print(fruit[3:-10])
print(fruit[3:99])
        </input>
  </program>
  <p>What do you think <c>fruit[:]</c> means?</p>
  <p>
    <term>Check your understanding</term>
  </p>
  <exercise label="test_question8_5_1">
    <statement>
      <p>What is printed by the following statements?</p>
      <program language="python">
        <input>
s = "python rocks"
print(s[3:8])
</input>
      </program>
    </statement>
    <choices>
      <choice>
        <statement>
          <p>python</p>
        </statement>
        <feedback><p>
                    That would be s[0:6].
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>rocks</p>
        </statement>
        <feedback><p>
                    That would be s[7:].
                </p></feedback>
      </choice>
      <choice correct="yes">
        <statement>
          <p>hon r</p>
        </statement>
        <feedback><p>
                    Yes, start with the character at index 3 and go up to but not include the character at index 8.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>Error, you cannot have two numbers inside the [ ].</p>
        </statement>
        <feedback><p>
                    This is called slicing, not indexing.  It requires a start and an end.
                </p></feedback>
      </choice>
    </choices>
  </exercise>
  <exercise label="test_question8_5_2">
    <statement>
      <p>What is printed by the following statements?</p>
      <program language="python">
        <input>
s = "python rocks"
print(s[7:11] * 3)
</input>
      </program>
    </statement>
    <choices>
      <choice correct="yes">
        <statement>
          <p>rockrockrock</p>
        </statement>
        <feedback><p>
                    Yes, rock starts at 7 and goes through 10.  Repeat it 3 times.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>rock rock rock</p>
        </statement>
        <feedback><p>
                    Repetition does not add a space.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>rocksrocksrocks</p>
        </statement>
        <feedback><p>
                    Slicing will not include the character at index 11.  Just up to it (10 in this case).
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>Error, you cannot use repetition with slicing.</p>
        </statement>
        <feedback><p>
                    The slice will happen first, then the repetition.  So it is ok.
                </p></feedback>
      </choice>
    </choices>
  </exercise>
  </subsection>
  <subsection xml:id="subsec-list-slice-ops">
    <title>List Slices</title>
    <p>The slice operation we saw with strings also work on lists.  Remember that the first index is the starting point for the slice and the second number is one index past the end of the slice (up to but not including that element).  Recall also
              that if you omit the first index (before the colon), the slice starts at the
              beginning of the sequence. If you omit the second index, the slice goes to the
              end of the sequence.</p>
    <program xml:id="chp09_6" interactive="activecode" language="python">
      <input>
  a_list = ['a', 'b', 'c', 'd', 'e', 'f']
  print(a_list[1:3])
  print(a_list[:4])
  print(a_list[3:])
  print(a_list[:])
          </input>
    </program>
    <p>
      <term>Check your understanding</term>
    </p>
    <exercise label="test_question9_6_1">
      <statement>
        <p>What is printed by the following statements?</p>
        <program language="python">
          <input>
  alist = [3, 67, "cat", [56, 57, "dog"], [ ], 3.14, False]
  print(alist[4:])
  </input>
        </program>
      </statement>
      <choices>
        <choice correct="yes">
          <statement>
            <p>[ [ ], 3.14, False]</p>
          </statement>
          <feedback><p>
                      Yes, the slice starts at index 4 and goes up to and including the last item.
                  </p></feedback>
        </choice>
        <choice>
          <statement>
            <p>[ [ ], 3.14]</p>
          </statement>
          <feedback><p>
                      By leaving out the upper bound on the slice, we go up to and including the last item.
                  </p></feedback>
        </choice>
        <choice>
          <statement>
            <p>[ [56, 57, "dog"], [ ], 3.14, False]</p>
          </statement>
          <feedback><p>
                      Index values start at 0.
                  </p></feedback>
        </choice>
      </choices>
    </exercise>
    </subsection>
  <note>
    <p>This workspace is provided for your convenience.  You can use this activecode window to try out anything you like.</p>
    <program xml:id="scratch_08_01" interactive="activecode" language="python">
      <input>


        </input>
    </program>
  </note>
</section>
