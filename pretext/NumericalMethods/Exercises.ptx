<?xml version="1.0"?>
<exercises xml:id="num_methods_exercises">
  <title>Exercises</title>
<exercise label="itWhile_MC_blastoff">
  <statement>

  <p>Consider the code block below. What happens when you run this program?</p>
  <program language="python"><input>
n = 5
while n &gt; 0:
print(n)
n = n + 1
print('Blastoff!')
</input></program>

  </statement>
<choices>

      <choice>
          <statement>
              <p>The program has five iterations but does not print anything.</p>
          </statement>
          <feedback>
              <p>Incorrect! The program both prints things and has more than five iterations. Try again.</p>
          </feedback>
      </choice>

      <choice correct="yes">
          <statement>
              <p>The loop will repeat forever and results in an infinite loop.</p>
          </statement>
          <feedback>
              <p>Correct! Because the loop is incrementing, rather than decrementing, n will always be greater than 0 and the loop will never end.</p>
          </feedback>
      </choice>

      <choice>
          <statement>
              <p>The program compiles and prints "1 2 3 4 5 Blastoff!" where each space is a new line.</p>
          </statement>
          <feedback>
              <p>Incorrect! The program will compile, but will not start with 1 and won't ever reach "Blastoff!". Try again.</p>
          </feedback>
      </choice>

      <choice>
          <statement>
              <p>The program compiles and prints "5 4 3 2 1 Blastoff!" where each space is a new line.</p>
          </statement>
          <feedback>
              <p>Incorrect! The program will compile, but it won't ever reach "Blastoff!". Try again.</p>
          </feedback>
      </choice>
</choices>
</exercise>

<exercise label="itWhile_fill1">
  <statement>
<p>Q-3: We call each time we execute the body of the loop an ________. <var/>  </p></statement><setup><var><condition string="[Ii]teration"><feedback><p>Each time we execute the body of a loop, we are completing an iteration.</p></feedback></condition><condition string=".*"><feedback><p>Incorrect! Read the text above this to find the answer. Try again!</p></feedback></condition></var></setup></exercise>        <p>The body of the loop should change the value of one or more variables so
  that eventually the condition becomes false and the loop terminates. We
  call the variable that changes each time the loop executes and controls
  when the loop finishes the <em>iteration variable</em>. If there
  is no iteration variable, the loop will repeat forever, resulting in an
  <em>infinite loop</em>.</p>

<exercise label="itWhile_MC_xy2">
<statement>

<p>Q-4: Consider the code block below. What are the values of x and y when this while loop finishes executing?</p>
<program language="python"><input>
x = 2
y = 5

while (y &gt; 2 and x &lt; y):
x = x + 1
y = y - 1
</input></program>

</statement>
<choices>

  <choice>
      <statement>
          <p>x = 2; y = 5</p>
      </statement>
      <feedback>
          <p>Incorrect! These were the values of x and y at first, but they changed by the time the loop finished executing. Try again.</p>
      </feedback>
  </choice>

  <choice>
      <statement>
          <p>x = 5; y = 2</p>
      </statement>
      <feedback>
          <p>Incorrect! The while loop will finish executing before x and y reach these values. Try again.</p>
      </feedback>
  </choice>

  <choice correct="yes">
      <statement>
          <p>x = 4; y = 3</p>
      </statement>
      <feedback>
          <p>Correct! The loop will terminate at x = 4 and y = 3 because at this point, x is not less than y.</p>
      </feedback>
  </choice>

  <choice>
      <statement>
          <p>x = 4; y = 2</p>
      </statement>
      <feedback>
          <p>Incorrect! The way the loop modifies x and y, it is impossible for y to be 2 while x is 4. Try again.</p>
      </feedback>
  </choice>
</choices>

</exercise>
<exercise label="viz_MC1">
  <statement>
    <p>csp-10-2-1: What is the standard way to import matplotlib's pyplot library in python?</p>
  </statement>
  <choices>
    <choice>
      <statement>
        <p>import matplot as plt</p>
      </statement>
      <feedback>
        <p>Incorrect! matplot is not a library.</p>
      </feedback>
    </choice>
    <choice correct="yes">
      <statement>
        <p>import matplotlib.pyplot as plt</p>
      </statement>
      <feedback>
        <p>Correct! This is the correct way to import the library.</p>
      </feedback>
    </choice>
    <choice>
      <statement>
        <p>from matplotlib import pyplot as plt</p>
      </statement>
      <feedback>
        <p>Incorrect! This is not the correct way to import.</p>
      </feedback>
    </choice>
    <choice>
      <statement>
        <p>import matplotlib pyplot as plt</p>
      </statement>
      <feedback>
        <p>Incorrect! There is a period between matplotlib and pyplot instead of space.</p>
      </feedback>
    </choice>
  </choices>
</exercise>
<exercise label="viz_MC2">
  <statement>
    <p>csp-10-2-2: What is the correct way to plot a line graph?</p>
  </statement>
  <choices>
    <choice>
      <statement>
        <p>plot(x, y)</p>
      </statement>
      <feedback>
        <p>Incorrect!</p>
      </feedback>
    </choice>
    <choice correct="yes">
      <statement>
        <p>plt.plot(x, y)</p>
      </statement>
      <feedback>
        <p>Correct! This is the correct way.</p>
      </feedback>
    </choice>
    <choice>
      <statement>
        <p>plt(x, y)</p>
      </statement>
      <feedback>
        <p>Incorrect!</p>
      </feedback>
    </choice>
    <choice>
      <statement>
        <p>plot.plt(x, y)</p>
      </statement>
      <feedback>
        <p>Incorrect!</p>
      </feedback>
    </choice>
  </choices>
</exercise>
<exercise label="viz_MC5">
  <statement>
    <p>csp-10-2-5: How to create a subplot in a figure with three plots side by side and two rows of subplots?</p>
  </statement>
  <choices>
    <choice>
      <statement>
        <p>ax1 = fig.add_subplot(123)</p>
      </statement>
      <feedback>
        <p>Incorrect! This means row 1, column 2, and plot 3, which is not possible in a 1 * 2 figure.</p>
      </feedback>
    </choice>
    <choice correct="yes">
      <statement>
        <p>ax1 = fig.add_subplot(231)</p>
      </statement>
      <feedback>
        <p>Correct! This is the correct way to create a subplot for this case.</p>
      </feedback>
    </choice>
    <choice>
      <statement>
        <p>ax1 = fig.add_subplot(321)</p>
      </statement>
      <feedback>
        <p>Incorrect! This means row 3, column 2 and plot 1. Row and column need to be switched.</p>
      </feedback>
    </choice>
    <choice>
      <statement>
        <p>ax1 = fig.add_subplot(3*2)</p>
      </statement>
      <feedback>
        <p>Incorrect! This is incorrect way to create subplot.</p>
      </feedback>
    </choice>
  </choices>
</exercise>
</exercises>
