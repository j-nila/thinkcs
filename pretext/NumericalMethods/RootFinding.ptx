<?xml version="1.0"?>
<section xml:id="root-finding">
  <title>Root finding</title>
  <introduction>
    <note>
      The examples in this section are inspired by <url href="https://patrickwalls.github.io/mathematicalpython/root-finding/root-finding/">Mathematical Python</url>
    </note>
  <p>Root finding refers to the general problem of searching for a solution of an equation <m>F(x)=0</m>
    for some function <m>F(x)</m>. This is a very general problem and it comes up a lot in mathematics, optimization, and machine learning problems.</p>
   
   <p>There are few cases where there exist exact methods for finding solutions. For example, the quadratic formula</p>
   <p><m>x = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a}</m></p>
   <p>gives us an exact formula for finding roots of the equation </p>
   <p><m>ax^2 + bx + c = 0</m></p>
   <p>But there does not exist a formula for many many polynomials and functions in general. Enter numerical methods to help find approximate solutions!</p>
   <p>(<url href="https://patrickwalls.github.io/mathematicalpython/root-finding/root-finding/">source</url>)</p>
   </introduction>
<subsection xml:id="subsec-roots-1">
  <title>Bisection method</title>
  <p>The bisection method is one of the simpler root-finding algorithms that is easy to understand and can be applied to any <term>continuous</term> function on an interval <m>[a, b]</m> where the value of <m>f(x)</m> will be 0 at some point (i.e., the function crosses the x-axis at some point in the interval). As such <m>f(a)f(b) \lt 0</m> since the function will be positive at one end and negative at the other, with a root in between</p>
  <image source='NumericalMethods/Figures/Bisection_method.svg.png'/>
  <p><url href ="https://en.wikipedia.org/wiki/File:Bisection_method.svg">(image from Wikipedia)</url></p>
  <term>The bisection algorithm:</term>
  <p>
    <ol>
      <li>
        <p>
          The bisection method finds the midpoint between the starting points <m>[a, b]</m>.
        </p>
      </li>
      <li>
        <p>
          If <m>f(midpoint) \approx 0</m> then you are done! Remember that we need to define equality with floating point numbers as close within a tolerance.
        </p>
        </li>
      <li>
        <p>
          If <m>f(midpoint)</m> has the same sign as <m>f(a)</m>, then the new <m>a</m>, or starting point of the interval becomes the midpoint. If it has the opposite sign, then it becomes the new <m>b</m>, or ending point. This ensures that the new interval continues to have a root within the interval since <m>f(a)f(b) \lt 0</m>.
        </p>
        </li>
        <li>
          <p>
            How do you know if <m>f(midpoint)</m> has the same sign as <m>f(a)</m>? If <m>f(a)f(midpoint) \gt 0</m>, then both are positive or both are negative - i.e., they have the same sign!
          </p>
        </li>
        <li>
          <p>
            You will continue computing the midpoint and checking if you are done or else changing the interval until you are done!
          </p>
          </li>
    </ol>
  </p>
<p><alert>Beware of floating point errors!</alert> This bears repeating - you may never find the root <m>x</m> where <m>f(x)</m> is <em>exactly</em> equal to zero. So we will end the algorithm when we are <em>close enough</em>, in other words within an acceptable tolerance. Did you know that <c>math</c> module has a function <c>isclose</c> that takes two values and returns True if they are within 1e-07% relative to the largest? Looks at the <url href="https://docs.python.org/3/library/math.html">documentation</url> if you want to specify your own tolerance with this function.</p>
<p>Do you know how many times you need to loop before you will find the answer? Once, twice, 100 times? It depends on a lot of factors! So we cannot use the <c>for</c> loop that we have used up until now. We need a new type of loop, where the loop will stop once our desired condition is reached. This is the <c>while</c> loop!</p>
</subsection>
<subsection xml:id="subsec-while">
  <title>The while loop</title>
<p>There is another Python statement that can also be used to build an iteration.  It is called the <c>while</c> statement.
  The <c>while</c> statement provides a much more general mechanism for iterating.  Similar to the <c>if</c> statement, it uses
  a boolean expression to control the flow of execution.  The body of while will be repeated as long as the controlling boolean expression evaluates to <c>True</c>.
</p>
<p>The following figure shows the flow of control.</p>
<image source="NumericalMethods/Figures/while_flow.png" width="50%"/>
<p>We can use the <c>while</c> loop to create any type of iteration we wish, including anything that we have previously done with a <c>for</c> loop.  For example, let us sum the numbers between 1 and 50 using <c>while</c>.
  Instead of relying on the <c>range</c> function to produce the numbers for our summation, we will need to produce them ourselves.  To do this, we will create a variable called <c>num</c> and initialize it to 1, the first number of the sum. 
  In order to control the iteration, we must have a boolean expression that evaluates to <c>True</c> as long as we want to keep adding values to our running total.  In this case, as long as <c>num</c> is less than or equal to the bound, we should keep going.</p>
<p>Here is a new version of the summation program that uses a while statement.</p>
<program interactive="activecode" language="python">
<input>
def sumTo(bound):
  """ Return the sum of 1+2+3 ... bound """
  sum  = 0
  num = 1
  while num &lt;= bound:
    sum = sum + num
    num = num + 1
  return sum

print(sumTo(4))
print(sumTo(50))
</input>
</program>
<p>You can almost read the <c>while</c> statement as if it were in natural language. It means,
  while <c>num</c> is less than or equal to <c>bound</c>, continue executing the body of the loop. Within
  the body, each time, update <c>sum</c> using the accumulator pattern and increment <c>num</c>. After the body of the loop, we go back up to the condition of the <c>while</c> and reevaluate it.  When <c>num</c> becomes greater than <c>bound</c>, the condition fails and flow of control continues to the <c>return</c> statement.</p>
<p>The same program in PythonTutor will allow you to observe the flow of execution.</p> <interactive iframe="https://j-nila.gitlab.io/pytutor/7.1while.html" width="100%"/>
<p>More formally, here is the flow of execution for a <c>while</c> statement:</p>
<p>
<ol marker="1">
<li>
<p>Evaluate the condition, yielding <c>False</c> or <c>True</c>.
</p>
</li>
<li>
<p>If the condition is <c>False</c>, exit the <c>while</c> statement and continue
          execution at the next statement.</p>
</li>
<li>
<p>If the condition is <c>True</c>, execute each of the statements in the body and
          then go back to step 1.</p>
</li>
</ol>
</p>
<p>The body consists of all of the statements below the header with the same
  indentation.</p>
<p>This type of flow is called a <term>loop</term> because the third step loops back around
  to the top. Notice that if the condition is <c>False</c> the first time through the
  loop, the statements inside the loop are never executed.</p>
<warning>
<p>Though Python's <c>while</c> is very close to the English <q>while</q>,
      there is an important difference:  In English <q>while X, do Y</q>,
      we usually assume that immediately after X becomes false, we stop
      with Y.  In Python there is <em>not</em> an immediate stop:  After the
      initial test, any following tests come only after the execution of
      the <em>whole</em> body, even if the condition becomes false in the middle of the loop body.</p>
</warning>
<p>The body of the loop should change the value of one or more variables so that
  eventually the condition becomes <c>False</c> and the loop terminates. Otherwise the
  loop will repeat forever. This is called an <term>infinite loop</term>.
  </p>
<p>In the case shown above, we can prove that the loop terminates because we
  know that the value of <c>bound</c> is finite, and we can see that the value of <c>num</c>
  increments each time through the loop, so eventually it will have to exceed <c>bound</c>. In
  other cases, it is not so easy to tell.</p>
<note>
<p>Introduction of the while statement causes us to think about the types of iteration we have seen.  The <c>for</c> statement will always iterate through a sequence of values like the list of names for the party or the list of numbers created by <c>range</c>.  Since we know that it will iterate once for each value in the collection, it is often said that a <c>for</c> loop creates a
<term>definite iteration</term> because we definitely know how many times we are going to iterate.  On the other
      hand, the <c>while</c> statement is dependent on a condition that needs to evaluate to <c>False</c> in order
      for the loop to terminate.  Since we do not necessarily know when this will happen, it creates what we
      call <term>indefinite iteration</term>.  Indefinite iteration simply means that we don't know how many times we will repeat but eventually the condition controlling the iteration should fail and the iteration will stop.</p>
</note>
<p>What you will notice here is that the <c>while</c> loop is more work for
  you &#x2014; the programmer &#x2014; than the equivalent <c>for</c> loop.  When using a <c>while</c>
  loop you have to control the loop variable yourself.  You give it an initial value, test
  for completion, and then make sure you change something in the body so that the loop
  terminates.</p>
<p>So why have two kinds of loop if <c>for</c> looks easier?  Remember our root-finding bisection algorithm? We need the indefinite iteration that we get from the <c>while</c> loop.</p>
</subsection>
<subsection xml:id="subsec-">
  <title>Returning to the bisection method</title>
  <p>Remember our algorithm:</p>
  <ol>
    <li><p>Choose a starting interval <m>[a, b]</m>
    such that <m>f(a)f(b) \lt 0 </m>. </p></li>
   <li><p>
   Compute <m>f(mid)</m> where <m>mid = (a+b)/2</m>
    is the midpoint.</p></li>
    <li><p>
   Determine the next subinterval <m>[a, b]</m>: 
   <ul>
    <li>
      <p>
        If <m>f(a)f(mid) \lt 0</m>, then <m>b</m> becomes <m>mid</m> for the next interval
        </p>
        </li>
      <li>
        <p>
          If <m>f(a)f(mid) \gt 0</m>, then <m>a</m> becomes <m>mid</m> for the next interval
          </p>
          </li>
          </ul>
          </p>
          </li>
    <li> 
      <p>
        Repeat (2) and (3) until <m> \left| f(mid) \right| \lt tolerance </m>.
          </p>
          </li>
    <li>
      <p>
        Return <m>mid</m> as the estimated root
      </p>
    </li>
   </ol>
   <exercise>
    <statement>
      <p>
        Complete the while loop in the function called bisection which takes 3 parameters f, a, and b, and returns the approximation of a solution to <m>f(x)=0</m> when <c>math.isclose(f(mid), 0)</c>. <alert>Hint: </alert><c>math.isclose</c> returns a boolean value: so we want to keep iterating through the loop as long as we are <c>not isclose(f(mid), 0)</c></p>.
        <note>You may be surprised to see the function <c>f</c> is a parameter to the <c>bisection</c> function. This is one of the strengths and beauty of Python! Functions are data types, just like ints or strings, so they can be passed to other functions. In this example, <c>polynomial</c> has already been defined as a function. Generally any function that represents a continuous mathematical function can be passed to <c>bisection</c> as a parameter.</note>
    </statement>
    <program interactive="activecode" language="python">
      <input>
import math
def polynomial(x):
  return x**2 - x - 1

def bisection(f, a, b):
  '''
  Approximate solution of f(x)=0 on interval [a,b] by bisection method.
  :param f: The function for which we are trying to approximate 
         a solution f(x)=0.
  :param a:one endpoint in the interval
  :param b: other endpoint in interval. f(a)*f(b) must be less than 0
  :return: estimate of root within the interval 
  '''
  mid = (a + b)/2
  f_mid = f(mid)
  while : #add the condition to repeat
    #your code here

  return mid

root = bisection(polynomial, -1, 2)
print(root, polynomial(root))

  </input>
    </program>
   </exercise>
 <p>You should find a root of approximately -0.6180339887498949.</p>
</subsection>
<note>
<p>This workspace is provided for your convenience.  You can use this activecode window to try out anything you like.</p>
<program xml:id="scratch_07_01" interactive="activecode" language="python">
<input>


</input>
</program>
</note>
<p>
<term>Check your understanding</term>
</p>
<exercise label="test_question7_2_1">
<statement>
<p>True or False: You can rewrite any for-loop as a while-loop.</p>
</statement>
<choices>
<choice correct="yes">
<statement>
<p>True</p>
</statement>
<feedback><p>
          Although the while loop uses a different syntax, it is just as powerful as a for-loop and often more flexible.
</p></feedback>
</choice>
<choice>
<statement>
<p>False</p>
</statement>
<feedback><p>
          Often a for-loop is more natural and convenient for a task, but that same task can always be expressed using a while loop.
</p></feedback>
</choice>
</choices>
</exercise>
<exercise label="test_question7_2_2">
<statement>
<p>The following code contains an infinite loop.  Which is the best explanation for why the loop does not terminate?</p>
<program language="python">
<input>
n = 10
answer = 1
while n &gt; 0:
  answer = answer + n
  n = n + 1
print(answer)
</input>
</program>
</statement>
<choices>
<choice correct="yes">
<statement>
<p>n starts at 10 and is incremented by 1 each time through the loop, so it will always be positive</p>
</statement>
<feedback><p>
          The loop will run as long as n is positive.  In this case, we can see that n will never become non-positive.
</p></feedback>
</choice>
<choice>
<statement>
<p>answer starts at 1 and is incremented by n each time, so it will always be positive</p>
</statement>
<feedback><p>
          While it is true that answer will always be positive, answer is not considered in the loop condition.
</p></feedback>
</choice>
<choice>
<statement>
<p>You cannot compare n to 0 in while loop.  You must compare it to another variable.</p>
</statement>
<feedback><p>
          It is perfectly valid to compare n to 0.  Though indirectly, this is what causes the infinite loop.
</p></feedback>
</choice>
<choice>
<statement>
<p>In the while loop body, we must set n to False, and this code does not do that.</p>
</statement>
<feedback><p>
          The loop condition must become False for the loop to terminate, but n by itself is not the condition in this case.
</p></feedback>
</choice>
</choices>
</exercise>
<exercise label="test_question7_2_3">
<statement>
<p>What is printed by this code?</p>
<program language="python">
<input>
n = 1
x = 2
while n &lt; 5:
  n = n + 1
  x = x + 1
  n = n + 2
  x = x + n
print(n, x)
</input>
</program>
</statement>
<choices>
<choice>
<statement>
<p>4 7</p>
</statement>
<feedback><p>
          Setting a variable so the loop condition would be false in the middle of the loop body does not keep the variable from actually being set.
</p></feedback>
</choice>
<choice>
<statement>
<p>5 7</p>
</statement>
<feedback><p>
          Setting a variable so the loop condition would be false in the middle of the loop body does not stop execution of statements in the rest of the loop body.
</p></feedback>
</choice>
<choice correct="yes">
<statement>
<p>7 15</p>
</statement>
<feedback><p>
          After n becomes 5 and the test would be False, but the test does not actually come until after the end of the loop - only then stopping execution of the repetition of the loop.
</p></feedback>
</choice>
</choices>
</exercise>
</section>
