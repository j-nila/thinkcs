<?xml version="1.0"?>
<section xml:id="VizLimits">
  <title>Visualizing Limits</title>
  <introduction>
    <p>We often need to determine the limit of a function <m>f(x)</m> as <m>x</m> approaches some number. If we plug in the number directly into the function, we typically get a result which is indeterminate; thus we need a different way to solve the function. If your differential calculus course, you are learning to solve this analytically with limit laws. We will look at numerical solutions to finding approximations to limits, using graphs.</p>
    </introduction>
  <subsection xml:id="subsec-limit-polynomial">
      <title>Finding an approximation to a limit</title>
      <p>Consider the function:</p>
      <p><m> f(x) = \frac{x^2 - 4x + 3}{1 - x} </m></p>
      <p>We would like to find the limit of this function as x approaches 1. If we just plug in 1 into the equation, we get 0 in the numerator and denominator, thus we must look at another technique. We will solve this numerically.</p>
        <p>We will first write a function called <c>f</c> which takes <c>x</c> as a parameter which returns the formula above.</p>
        <p>We then write a function <c>limit</c> that will perform the following mathemetical action: </p>
        <p><m>\lim_{x \to N^-}f(x)</m> to find the left-hand limit.</p>
        <p>The function <c>limit</c>  takes three parameters, <c>N</c> that represents the value that x is approaching, <c>start</c> that represents the starting value from which you approach N, and <c>delta_x</c> which indicates how far apart each x value is. This function will keep calling <m>f(x)</m>, getting closer to the limit N by delta_x on each iteration. The resulting f(x) values are saved in a list and returned by the function.</p>
        <p>Questions to ask before we code this:
          <ul>
            <li>
              <p>
                how many iterations through the loop are required? Do we know ahead of time?
              </p>
            </li>
            <li>
              <p>
                will each value of x be a whole number? Or are there possibly fractional x-values?
              </p>
            </li>
          </ul>
        </p>
        <p>We can determine the number of iterations as <m>\frac{|N-start|}{delta_x}</m> but this is a float (since / always results in a float) so it cannot be used directly in a range. Also be careful if you use a range, since delta_x is not the step value!</p>
<program xml:id="limits-1" interactive="activecode" language="python">
  <input>
def f(x):
  '''
  Function representing the function for which we want to find the limit
  :param x: the x value
  :return the corresponding y value
  '''
  y = (x**2 - 4*x + 3)/(1 - x)
  return y


def limit(N, start, delta_x):
  '''
  Returns a list of f(x) values as x approaches N
  :param N the limit being approached
  :param start the starting point to the left
  :param delta_x the size of the step
  :return list of f(x)
  '''

  y_values = []
  x = start;
  while x &lt; N:
    y = f(x)
    y_values.append(y)
    x = x + delta_x
  return y_values

left_start = -1
delta = 0.001
left_list = limit(1, left_start, delta)
print(left_list)

# in Trinket
#import matplotlib.pyplot as plt
#xaxis = []
#x = left_start
#while x &lt; 1:
#  xaxis.append(x)
#  x = x + delta
#xaxis.append(1)
#for i in range(steps):
#  x = x + delta
#  xaxis.append(x)
#plt.plot(xaxis, left_list, 'b-')
#plt.xlabel('x')
#plt.ylabel('f(x)')
#plt.show()
  </input>
</program>
<interactive iframe="https://trinket.io/features/python3" width="100%"/>
<p>Looking at the list and extrapolating the graph, it appears that the limit is 2. This corresponds to the analytical solution found by factoring:</p>
<p><m>\lim_{x \to 1^-} \frac{x^2 - 4x + 3}{1 - x} </m></p>
<p><m>\lim_{x \to 1^-} \frac{(x-1)(x-3)}{-(x-1)} </m></p>
<p><m>\lim_{x \to 1^-} -(x-3)</m> for <m>x \neq 1 </m></p>
<p><m>-(1-3) = 2 </m></p>
        </subsection>
<p><alert>Playing with the code</alert></p>
<p>Change <c>f(x)</c> to represent <m>\frac{1}{x}</m>, and try to estimate the left-hand limit and the right-hand limit as x approaches 0. You will need to add if statements to the <c>limit</c> function so the the start on a right-hand limit is <c>N + delta_x</c>. If the two limits are not the same, you can conclude that the limit diverges. </p>
</section>