<?xml version="1.0"?>
<section xml:id="DifferenceEqs">
  <title>Difference Equations</title>
  <introduction>
    <note>
      The examples in this section are from <url href="https://github.com/hplgit/primer.html/blob/master/doc/pub/all/book.pdf">A Primer on Scientific
        Programming with Python</url> Appendix A: Sequences and difference equations, by Hans Petter Langtangen.
    </note>
    <p>From mathematics you probably know the concept of a sequence, which
      is nothing but a collection of numbers with a specific order. A general
      sequence is written as</p>
    <p><m>x_0, x_1, x_2, . . . , x_n, . . . ,</m></p>
      <p>One example is the sequence of all odd numbers:</p>
      <p><m>1, 3, 5, 7, . . . , 2n + 1, . . .</m>. </p>
    <p>For some sequences it is not so easy to set up a general formula for the
      <m>n</m>-th term. Instead, it is easier to express a relation between two or more
      consecutive elements. One example where we can do both things is the
      sequence of odd numbers. This sequence can alternatively be generated
      by the formulaL</p>
      <p><m>x_{n+1} = x_n + 2</m>.</p>
      <p>To start the sequence, we need an initial condition where the value of
        the first element is specified:</p>
        <p><m>x_0 = 1</m>.</p>
      <p>Relations between consecutive elements in a sequence is called
        recurrence relations or <term>difference equations</term>. Solving a difference equation
        can be quite challenging in mathematics, but it is almost trivial to solve
        it on a computer. That is why difference equations are so well suited for
        computer programming, and the present section is devoted to this topic.
        Necessary background knowledge is programming with loops, lists, and
        visualization of a function of one variable.</p>
        </introduction>
  <subsection xml:id="subsec-finite-difference">
      <title>Interest rates</title>
      <p>Our first difference equation model concerns how much money an initial
      amount <m>x_0</m> will grow to after <m>n</m> years in a bank with annual interest rate <m>p</m> (e.g., 6%, and not 0.06). You may have learned in school the formula: </p>
      <p><m>x_n = x_0\left(1 + {p\over 100}\right)^n</m></p>
      <p>Unfortunately, this formula arises after some limiting assumptions, like
        that of a constant interest rate over all the <m>n</m> years. Moreover, the formula
        only gives us the amount after each year, not after some months or days.
        It is much easier to compute with interest rates if we set up a more
        fundamental model in terms of a difference equation and then solve this
        equation on a computer.</p><p>
        The fundamental model for interest rates is that an amount <m>x_{n-1}</m> at
        some point of time <m>t_{n-1}</m> increases its value with <m>p</m> percent to an amount
        <m>x_{n}</m> at a new point of time <m>t_{n}</m>:</p>
        <p><m>x_{n} = x_{n-1} + {p\over 100}x_{n-1}</m></p>
        <p>If <m>n</m> counts years, <m>p</m> is the annual interest rate, and if <m>p</m>
          is constant, we can with some arithmetics derive the following solution:</p>
      <p><m>x_{n} = \left( 1+{p\over100}\right)x_{n-1}=
        \left( 1+{p\over100}\right)^2 x_{n-2} =\ldots=
        \left( 1+{p\over100}\right)^{n}x_0</m></p>
      <p>Instead of first deriving a formula for <m>x_n</m> and then program this
        formula, we may attack the fundamental model in a program  and compute
        <m>x_1</m>, <m>x_2</m>, and so on in a loop.</p>
   <program xml:id="interest-1" interactive="activecode" language="python">
    <input>
# money over years
x = []
# initial amount
x.append(100)
# interest rate            
p = 5
# number of years
N = 4
# Compute solution
for n in range(N):
   x.append(x[n] + (p/100.0)*x[n])

print(x)

# in Trinket
#import matplotlib.pyplot as plt
#plt.plot(range(N+1), x, 'ro')
#plt.xlabel('years')
#plt.ylabel('amount')
#plt.show()
       </input>
</program> 
<p>Suppose now that we are interested in computing the growth of money
  after <m>N</m> days instead. The interest rate per day is taken as <m>r = p/D</m> if <m>p</m>  is the annual interest rate and <m>D</m> is the number of days in a year. The
  fundamental model is the same, but now <m>n</m> counts days and <m>p</m> is replaced
  by <m>r</m>:</p>
  <p><m>x_{n} = x_{n-1} + {r\over 100}x_{n-1}</m></p>
  <p>Python has a module
    <c>datetime</c> for convenient calculations with dates and times. To find the
    number of days between two dates, we can perform the following operations:</p>
    <program language="python">
      <input>
import datetime
date1 = datetime.date(2020, 8, 21)
date2 = datetime.date(2024, 8, 21)
diff = date2 - date1
print(diff.days)  
      </input>
    </program>
<p>We can now modify the previous program to compute with days instead of
  years:</p>
  <program xml:id="interest-2" interactive="activecode" language="python">
    <input>
import datetime

# money increasing over days
x = []
# initial amount
x.append(100)
# annual interest rate            
p = 5
# daily interest rate
r = p/365
# number of days
date1 = datetime.date(2020, 8, 21)
date2 = datetime.date(2024, 8, 21)
diff = date2 - date1
N = diff.days
# Compute solution
for n in range(N):
   x.append(x[n] + (r/100.0)*x[n])

print(x)

# in Trinket
#import matplotlib.pyplot as plt
#plt.plot(range(N+1), x, 'ro')
#plt.xlabel('days')
#plt.ylabel('amount')
#plt.show()
       </input>
</program>  
<p>It was easy to adjust the formula to the case where the
  interest is added every day instead of every year. However, the strength
  of the model and the associated program becomes
  apparent when <m>r</m> <alert>varies in time</alert> - and this is what happens in real life. In
  the model we can just write <m>r(n)</m> to explicitly indicate the dependence
  upon time. The corresponding time-dependent annual interest rate is
  what is normally specified, and <m>p(n)</m> is usually a piecewise constant
  function (the interest rate is changed at some specific dates and remains
  constant between these days). For now we assume that
  a list <m>p</m> holds the time-dependent annual interest rates for each day
  in the total time period being examined. The program then
  needs a slight modification, typically:</p>
  <program xml:id="interest-3" interactive="activecode" language="python">
    <input>
import datetime

# money increasing over days
x = []
# initial amount
x.append(100)
# annual interest rate is now a list of annual rate
# on each day. Only 10 days in this example. 
# setting up this list may be challenging!           
p = [5, 5, 6, 6, 6, 6, 6, 6, 5.5, 5.5]
# daily interest rates
r = []
for rate in p:
  r.append(rate/365)
# number of days
date1 = datetime.date(2024, 8, 11)
date2 = datetime.date(2024, 8, 20)
diff = date2 - date1
N = diff.days
# Compute solution
for n in range(N):
   x.append(x[n] + (r[n]/100.0)*x[n])

print(x)

# in Trinket
#import matplotlib.pyplot as plt
#plt.plot(range(N+1), x, 'ro')
#plt.xlabel('days')
#plt.ylabel('amount')
#plt.show()
       </input>
</program>  
<p>A difference equation with <m>r(n)</m> is quite difficult to solve mathematically,
  but the <m>n</m>-dependence (ot time dependance) in <m>r</m> is easy to deal with in the computerized
  solution approach.</p>
     </subsection>
</section>