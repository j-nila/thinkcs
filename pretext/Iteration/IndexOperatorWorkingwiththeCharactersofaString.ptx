<?xml version="1.0"?>
<section xml:id="index-operator-working-with-elements">
  <title>Index Operator: Accessing elements of a collction</title>
  <p>The <term>indexing operator</term> <c>[ ]</c> (Python uses square brackets to enclose the index)
            selects a single character from a string, or a single element of a list.  They are accessed by their position or
            <term>index</term> value.  For example, in the string shown below, the 14 characters are indexed left to right from position 0 to position 13.<alert>Notice that counting of position always starts at 0, not 1!</alert></p>
  <image source="Strings/Figures/indexvalues.png" width="50%" alt="index values"/>
  <p>It is also the case that the positions are named from right to left using negative numbers where -1 is the rightmost
            index and so on.
            Note that the character at index 6 (or -8) is the blank character.</p>
  <program xml:id="chp08_index1-2" interactive="activecode" language="python">
    <input>
school = "Luther College"
letter = school[2]
print(letter)

lastchar = school[-1]
print(lastchar)

cs_teachers = ['Jaya', 'Louisa', 'Eric']
print(cs_teachers[0])
        </input>
  </program>
  <p>The expression <c>school[2]</c> selects the character at index 2 (in other words, the third character) from <c>school</c>, and creates a new
            string containing just this one character. The variable <c>m</c> refers to the result.</p>
  <note>Remember that programmers <alert>start counting
            from zero</alert>. The letter at index zero of <c>"Luther College"</c> is <c>L</c>.  So at
            position <c>[2]</c> we have the letter <c>t</c>.</note>
  <p>If you want the zero-eth letter of a string, you put 0, or any expression
            with the value 0, in the brackets.  Give it a try.</p>
  <p>The expression in brackets is called an <term>index</term>. An index specifies a member
            of an ordered collection.  In this case the collection of characters in the string. The index
            <em>indicates</em> which character you want. It can be any integer
            expression that evaluates to a valid index value.</p>
  <p>Note that indexing a string returns a <em>string</em> &#x2014; Python has no special type for a single character.
            It is just a string of length 1.</p>
  <p>
    <term>Check your understanding</term>
  </p>
  <exercise label="test_question8_2_1_2">
    <statement>
      <p>What is printed by the following statements?</p>
      <program language="python">
        <input>
s = "python rocks"
print(s[3])
</input>
      </program>
    </statement>
    <choices>
      <choice>
        <statement>
          <p>t</p>
        </statement>
        <feedback><p>
                    Index locations do not start with 1, they start with 0.
                </p></feedback>
      </choice>
      <choice correct="yes">
        <statement>
          <p>h</p>
        </statement>
        <feedback><p>
                    Yes, index locations start with 0.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>c</p>
        </statement>
        <feedback><p>
                    s[-3] would return c, counting from right to left.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>Error, you cannot use the [ ] operator with a string.</p>
        </statement>
        <feedback><p>
                    [ ] is the index operator
                </p></feedback>
      </choice>
    </choices>
  </exercise>
  <exercise label="test_question8_2_2">
    <statement>
      <p>What is printed by the following statements?</p>
      <program language="python">
        <input>
s = "python rocks"
print(s[2] + s[-5])
</input>
      </program>
    </statement>
    <choices>
      <choice correct="yes">
        <statement>
          <p>tr</p>
        </statement>
        <feedback><p>
                    Yes, indexing operator has precedence over concatenation.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>ps</p>
        </statement>
        <feedback><p>
                    p is at location 0, not 2.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>nn</p>
        </statement>
        <feedback><p>
                    n is at location 5, not 2.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>Error, you cannot use the [ ] operator with the + operator.</p>
        </statement>
        <feedback><p>
                    [ ] operator returns a string that can be concatenated with another string.
                </p></feedback>
      </choice>
    </choices>
  </exercise>
</section>
