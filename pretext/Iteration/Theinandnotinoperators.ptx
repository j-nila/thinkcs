<?xml version="1.0"?>
<section xml:id="the-in-and-not-in-operators">
  <title>The <c>in</c> and <c>not in</c> operators</title>
  <p>The <c>in</c> operator tests if something is a member of a collection.</p>
  <p>The <c>not in</c> operator returns the logical opposite result of <c>in</c>.</p>
  <p>Here are some examples with lists and strings.</p>
  <program xml:id="chp8_in1" interactive="activecode" language="python">
    <input>
print('p' in 'apple')
print('i' in 'apple')
print('ap' in 'apple')
print('pa' in 'apple')
print('Jaya' in ['Louisa', 'Eric', 'Jaya'])
print('Jay' in ['Louisa', 'Eric', 'Jaya'])
        </input>
  </program>
  <p>Strings are special! Note that a string is a substring of itself, and the empty string is a
            substring of any other string. (Also note that computer scientists
            like to think about these edge cases quite carefully!).</p>
  <program xml:id="chp8_in2" interactive="activecode" language="python">
    <input>
print('a' in 'a')
print('apple' in 'apple')
print('' in 'a')
print('' in 'apple')
        </input>
  </program>
  <p>The <c>not in</c> operator returns the logical opposite result of <c>in</c>.</p>
  <program xml:id="chp8_in3" interactive="activecode" language="python">
    <input>
print('Lei' not in ['Louisa', 'Eric', 'Jaya'])
print('x' not in 'apple')
        </input>
  </program>
</section>
