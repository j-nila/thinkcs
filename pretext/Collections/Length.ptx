<?xml version="1.0"?>
<section xml:id="strings_length">
  <title>Length</title>
  <p>The <c>len</c> function, when applied to a string, returns the number of characters in a string; when applies to a list, it returns the number of elements.</p>
  <program xml:id="chp08_len1" interactive="activecode" language="python">
    <input>
fruits = ['apple', 'banana', 'mango', 'pineapple']
fruit = fruits[1]
print(f'The list fruits contains {len(fruits)}, and the second fruit has {len(fruit)} letters)
        </input>
  </program>
  <p>To get the last item of a list or letter of a string, you might be tempted to try something like
            this:</p>
  <program xml:id="chp08_len2" interactive="activecode" language="python">
    <input>
fruit = 'banana'
length = len(fruit)
last_letter = fruit[length] # ERROR!

fruits = ['apple', 'banana', 'mango', 'pineapple']
size = len(fruits)
last = fruit[size]       # ERROR!
        </input>
  </program>
  <p>Both won't work. They cause the runtime error
            <c>IndexError</c>. The reason is that there is nothing
            at index position 6 in <c>"Banana"</c> or 4 in the list.
            Since we started counting at zero, the six indexes of banana are
            numbered 0 to 5, and the list are numbered 0 to 3. To get the last item, we have to <em>subtract 1</em> from
            the length.  Fix the example above.</p>
  <program xml:id="ch08_len3" interactive="activecode" language="python">
    <input>
fruit = 'banana'
length = len(fruit)
last_letter = fruit[length - 1] 
      
fruits = ['apple', 'banana', 'mango', 'pineapple']
size = len(fruits)
last = fruit[size - 1]       
        </input>
  </program>
  <p>Alternatively in Python, we can use <term>negative indices</term>, which count backward from the
            end of the string. The expression <c>fruit[-1]</c> yields the last letter,
            <c>fruit[-2]</c> yields the second to last, and so on.  Try it!
            Most other languages do <em>not</em> allow the negative indices, but they are a handy feature of Python!</p>
  <p>
    <term>Check your understanding</term>
  </p>
  <exercise label="test_question8_4_1">
    <statement>
      <p>What is printed by the following statements?</p>
      <program language="python">
        <input>
s = "python rocks"
print(len(s))
</input>
      </program>
    </statement>
    <choices>
      <choice>
        <statement>
          <p>11</p>
        </statement>
        <feedback><p>
                    The blank counts as a character.
                </p></feedback>
      </choice>
      <choice correct="yes">
        <statement>
          <p>12</p>
        </statement>
        <feedback><p>
                    Yes, there are 12 characters in the string.
                </p></feedback>
      </choice>
    </choices>
  </exercise>
  <exercise label="test_question8_4_2">
    <statement>
      <p>What is printed by the following statements?</p>
      <program language="python">
        <input>
s = "python rocks"
print(s[len(s)-5])
</input>
      </program>
    </statement>
    <choices>
      <choice>
        <statement>
          <p>o</p>
        </statement>
        <feedback><p>
                    Take a look at the index calculation again, len(s)-5.
                </p></feedback>
      </choice>
      <choice correct="yes">
        <statement>
          <p>r</p>
        </statement>
        <feedback><p>
                    Yes, len(s) is 12 and 12-5 is 7.  Use 7 as index and remember to start counting with 0.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>s</p>
        </statement>
        <feedback><p>
                    s is at index 11
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>Error, len(s) is 12 and there is no index 12.</p>
        </statement>
        <feedback><p>
                    You subtract 5 before using the index operator so it will work.
                </p></feedback>
      </choice>
    </choices>
  </exercise>
  <exercise label="test_question8_4_3">
    <statement>
      <p>What is printed by the following statements?</p>
      <program language="python">
        <input>
s = "python rocks"
print(s[-3])
</input>
      </program>
    </statement>
    <choices>
      <choice correct="yes">
        <statement>
          <p>c</p>
        </statement>
        <feedback><p>
                    Yes, 3 characters from the end.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>k</p>
        </statement>
        <feedback><p>
                    Count backward 3 characters.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>s</p>
        </statement>
        <feedback><p>
                    When expressed with a negative index the last character s is at index -1.
                </p></feedback>
      </choice>
      <choice>
        <statement>
          <p>Error, negative indices are illegal.</p>
        </statement>
        <feedback><p>
                    Python does use negative indices to count backward from the end.
                </p></feedback>
      </choice>
    </choices>
  </exercise>
</section>
